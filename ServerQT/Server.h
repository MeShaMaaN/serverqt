#pragma once
#include <boost\asio.hpp>
#include <boost\array.hpp>
#include <boost\thread.hpp>
#include <boost\lexical_cast.hpp>

// переменные
const uint32_t dlen = 400000;
boost::array<uint32_t, 1024000> buf;
bool syncMode;
boost::asio::ip::tcp::acceptor* acceptor2;
boost::asio::io_service* io_service2; 
void syncServer(void *);
void ServiceThread(void);
void ServiceThread24(void);
int Init14bitADC(void);
void PrintConfig(void);
void ParseFile(void);
BOOL SetNewTime(WORD, WORD, WORD, WORD);