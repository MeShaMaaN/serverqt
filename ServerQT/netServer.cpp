#include "netServer.h"
#include <boost\array.hpp>
#include <boost\thread.hpp>
//#include "windows.h"	// ��� ����, ����� ������� �������������� ������� ������������ �����������
#include <vector>
#include <array>
#include <stdio.h>
#include <conio.h>
#include <clocale>
#include <iostream>
#include <iomanip>
#include <objbase.h>
#include <math.h>
#include <time.h>


//#define thread_cout(msg)\ PCout::Mutex().lock(), msg, PCout::Mutex().unlock()

using namespace std;
using namespace boost;
using namespace boost::asio;
using boost::asio::ip::tcp;

extern int16_t buf16[1000000];		// ����� ��� �������� ����� ������� ����� � ��� � �������� ��������
extern uint32_t counter;	
extern int32_t buf32[32][25000];		// ����� ��� �������� ����� ������� ����� � ��� � �������� ��������

extern double adc14_freq;	// ������� ���
extern int adc14_pages;		// ���������� ������� ������ ��� �� 6144 
extern int adc_cycles;		// ���������� �������� ��� ���������� ������
extern int adc14_channels;	// ���������� ������� ���

extern string gps_port;	// ���� ��� ����������� GPS
extern int port_speed;
extern bool gps_data_save;
extern bool nmea_data_save;


extern int adc_type;
extern int channel_from;
extern int channel_to;
extern int shot_period;
extern float sample_interval;
extern float record_lenght;
extern int record_delay;
extern int depth_port;


string csasServer::processData(char* in)
{
	cout << "������ �������: " << in << endl;
	return syncMode ? "Sync server online" : "Async server online";
}
/*csasServer::csasServer(void)
{
	
}
*/
csasServer::~csasServer(void)
{
	if (acceptor)delete acceptor;
	if (io_service)delete io_service;
}

void csasServer::startServer(const char* server, const char* port, bool syncmode)
{
	syncMode = syncmode;
	io_service = new boost::asio::io_service();
	// ������� �������� �����������
	acceptor = new tcp::acceptor(*io_service, tcp::endpoint(boost::asio::ip::tcp::v4(), atoi(port)));
	if (syncMode)
	{
		// ���������� ������				
		startSyncServer();
		
	}
	else
	{
		// ����������� ������
		cout << "AsyncServerStarted" << endl;
		startAsyncServer();
	}
}


void csasServer::startSyncServer()
{
	for (;;)
	{
		boost::system::error_code error;
		uint16_t ask;
		string msg("");
		// ������� �����������	
		tcp::socket socket(*io_service);
		acceptor->accept(socket, error);
		// �������� ������
		const uint32_t dlen = 512;
		boost::array<uint32_t, 400000> buf;

		size_t len = socket.read_some(boost::asio::buffer(buf), error);
		// ������-���������� ����� ������ � std::�out
		
			cout << "������ �������: " << len << " ����" << endl;
			cout << "������: " << ask << endl;
			
		memcpy(&ask, &buf, 2);
		
		// ���������� ������
		/*for (int32_t i = 0; i < 1000; i++)
		if (i * 16 + counter < 16000)
		buf[i] = i * 16 + counter;
		else
		buf[i] = (i * 16 + counter) - 16000;
		counter += 16;
		*/
		for (uint32_t i = 0; i < 10000; i++)
		{
			buf[i] = buf16[i] & 0x3fff;
			/*buf[i] = buf16[i] >> 8;
			buf[i] += (buf16[i] << 8 )& 0xFF00;*/
		}
		// ������� ���������
		boost::asio::write(socket, boost::asio::buffer(buf), error);
		// �����������
		socket.close();
	}
}


/*=========================== ��������� ����������� ������ ������� =================================*/

void csasServer::write_handler(const boost::system::error_code& error, std::size_t bytes_transferred, tcp::socket* socket)
{
	cout << "�������� �������" << endl;
	// ��������� ������
	socket->close();
	delete socket;
}

void csasServer::read_handler(const boost::system::error_code& error, std::size_t bytes_transferred, char* msg, tcp::socket* socket)
{



	// ���������� ������
	for (uint32_t i = 0; i < 100000; i++)
		if (i / 2000 + counter < 16000)
			buf[i] = i / 2000 + counter;
		else
			buf[i] = (i / 2000 + counter) - 16000;
	counter += 16;
	cout << "�������: " << (error.value() == 0 ? "" : error.message()) << " " << msg << " ������� ����: " << bytes_transferred << endl;
	delete msg;
	// ������ ��������� �������� ������
	boost::asio::async_write(*socket, boost::asio::buffer(buf, 40000), boost::bind(&csasServer::write_handler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, socket));
}

// ������� ���������� ����� ������
std::size_t csasServer::completion_condition(const boost::system::error_code& error, std::size_t bytes_transferred, char* buf)
{
	return bytes_transferred<128 ? 8 : 0;
}

void csasServer::acc(tcp::socket* socket, const boost::system::error_code& error)
{
	const int dlen = 256;
	char* bufx = new char[dlen];
	//boost::array<char, dlen> buf;
	boost::asio::async_read(*socket, boost::asio::buffer(bufx, dlen), boost::bind(&csasServer::completion_condition, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, bufx), boost::bind(&csasServer::read_handler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, bufx, socket));
	//cout<<"�������"<<bufx.c_array()<<endl;
}

/* =============================== ����� ================================= */
void csasServer::startAsyncServer()
{
	// ����� ������� �������������� �������, �� ��� ����� ������ ���������� ������-�� �������: fatal error C1001: ���������� ������ � ����� �����������.(���� ����������� "msc1.cpp", ������ 1420)
	//std::auto_ptr<tcp::socket> socket(new tcp::socket(*io_service));
	// ���������������� ����� - ����� ��������� � �����������		
	try
	{
		for (;;)
		{
			tcp::socket* socket = new tcp::socket(*io_service);
			// ���������� ����������� ��������		
			// ����� ����������� ������� - ����������� ����������, ������� �� ������� � ������������ ��� ��������� ����������		
			//acceptor->async_accept((*socket),boost::bind(&csasServer::asyncCompleteOperationHandle,this,1,socket, (char*)"", boost::asio::placeholders::error));				

			acceptor->async_accept((*socket), boost::bind(&csasServer::acc, this, socket, boost::asio::placeholders::error));

			// ��������� ���� ���������

			io_service->run();
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	cout << "Exit" << endl;
}
