#include "serial_port2.h"
#include <iostream>

#include "nmea.h"
#include <windows.h>
//#include <Setupapi.h>



#pragma comment(lib, "Setupapi.lib")
//NMEAParser GPSparse("gps_pars.txt");

using namespace std;

extern string gps_port;	// ���� ��� ����������� GPS
extern int port_speed;
extern int gps_data_save;
extern int nmea_data_save;
extern string gps_file_name;	// 
extern string nmea_file_name;	// 
extern s_gps last_gps;			// ��������� �������� GPS ������

extern string time_sentence;	// �������� GPS ����� (RMC, GGA, ZDA, VTG, GLL)
extern string coord_sentence;
extern string course_sentence;

#include "nmea.h"


NMEA nm(0);

SerialPort::SerialPort(void) : end_of_line_char_('\n')
{
}

SerialPort::~SerialPort(void)
{
	stop();
}

char SerialPort::end_of_line_char() const
{
	return this->end_of_line_char_;
}

void SerialPort::end_of_line_char(const char &c)
{
	this->end_of_line_char_ = c;
}

bool SerialPort::start(const char *com_port_name, int baud_rate)
{
	boost::system::error_code ec;

	if (port_) {
		std::cout << "error : port is already opened..." << std::endl;
		return false;
	}

	port_ = serial_port_ptr(new boost::asio::serial_port(io_service_));
	port_->open(com_port_name, ec);
	if (ec) {
		std::cout << "error : port_->open() failed...com_port_name="
			<< com_port_name << ", e=" << ec.message().c_str() << std::endl;
		return false;
	}

	// option settings...
	port_->set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
	port_->set_option(boost::asio::serial_port_base::character_size(8));
	port_->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
	port_->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
	port_->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));

	boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service_));

	async_read_some_();

	return true;
}

void SerialPort::stop()
{
	boost::mutex::scoped_lock look(mutex_);

	if (port_) {
		//port_->cancel();
		port_->close();
		port_.reset();
	}
	io_service_.stop();
	io_service_.reset();
}

int SerialPort::write_some(const std::string &buf)
{
	return write_some(buf.c_str(), buf.size());
}

int SerialPort::write_some(const char *buf, const int &size)
{
	boost::system::error_code ec;

	if (!port_) return -1;
	if (size == 0) return 0;

	return port_->write_some(boost::asio::buffer(buf, size), ec);
}

void SerialPort::async_read_some_()
{
	if (port_.get() == NULL || !port_->is_open()) return;

	port_->async_read_some(
		boost::asio::buffer(read_buf_raw_, SERIAL_PORT_READ_BUF_SIZE),
		boost::bind(
			&SerialPort::on_receive_,
			this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
}

void SerialPort::on_receive_(const boost::system::error_code& ec, size_t bytes_transferred)
{
	boost::mutex::scoped_lock look(mutex_);

	if (port_.get() == NULL || !port_->is_open()) return;
	if (ec) {
		async_read_some_();
		return;
	}

	for (unsigned int i = 0; i < bytes_transferred; ++i) {
		char c = read_buf_raw_[i];
		if (c == end_of_line_char_) {
			this->on_receive_(read_buf_str_);
			read_buf_str_.clear();
		}
		else {
			read_buf_str_ += c;
		}
	}

	async_read_some_();
}

void SerialPort::on_receive_(const std::string &data)
{
	string str = data;	// ������ ��� ������������ ������ � ���� GPS
	int bytes_transferred = str.length();
	char parser_result;	// ��� ������������� ������
	//std::cout << bytes_transferred << " bytes: " << read_msg_ << std::endl;
	//string str = read_msg_;

	if (nmea_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
		NMEA_File = fopen(nmea_file_name.c_str(), "a");
		fwrite(str.c_str(), bytes_transferred, 1, NMEA_File);
		fclose(NMEA_File);
	}
	for (int i = 0; i < bytes_transferred; i++)
	{
		parser_result = nm.decode(str[i]);
		switch (parser_result) {
		case 'R':		// GPRMC
			if (cur_time != (int)nm.gprmc_utc())
			{
				
				if (course_sentence == "RMC") {
					last_gps.lock.lock();
					last_gps.course = nm.gprmc_course();
					last_gps.speed = nm.gprmc_speed(1);	// � �����
					last_gps.course_string = "RMC course: " + to_string(last_gps.course) + " speed: " + to_string(last_gps.speed) + "\n";
					last_gps.lock.unlock();
					if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
						GPS_File = fopen(gps_file_name.c_str(), "a");
						fwrite(last_gps.course_string.c_str(), last_gps.course_string.length(), 1, GPS_File);
						fclose(GPS_File);
					}
				}
				if (coord_sentence == "RMC") {
					last_gps.lock.lock();
					last_gps.latitude = nm.gprmc_latitude();
					last_gps.longitude = nm.gprmc_longitude();
					last_gps.coord_string = to_string(nm.gprmc_latitude()) + " " + to_string(nm.gprmc_longitude()) + "\n";
					last_gps.lock.unlock();
					if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
						GPS_File = fopen(gps_file_name.c_str(), "a");
						fwrite(last_gps.coord_string.c_str(), last_gps.coord_string.length(), 1, GPS_File);
						fclose(GPS_File);
					}
				}
				if (time_sentence == "RMC") {
					last_gps.lock.lock();
					last_gps.time = nm.gprmc_utc();
					uint32_t xx = last_gps.time;
					last_gps.ms = (float)(last_gps.time - xx) * 1000;
					last_gps.ss = xx % 100;
					last_gps.mm = (xx - last_gps.ss) / 100 % 100;
					last_gps.hh = (xx - last_gps.mm - last_gps.ss) / 10000 % 100;
					last_gps.time_string = to_string(last_gps.hh) + ":" + to_string(last_gps.mm) +":" + to_string(last_gps.ss) + "." + to_string(last_gps.ms) + " ";
					cur_time = (int)nm.gprmc_utc();	//  ���������� ��������� ��������, ����� �� �����������
					last_gps.lock.unlock();
					if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
						GPS_File = fopen(gps_file_name.c_str(), "a");
						fwrite(last_gps.time_string.c_str(), last_gps.time_string.length(), 1, GPS_File);
						fclose(GPS_File);
					}
				}
			}
			break;
		case 'G':		// GPGGA
			if (1)//nm.gpgga_quality() != 0)
			{
				
				if (coord_sentence == "GGA") {
					last_gps.lock.lock();
					last_gps.latitude = nm.gpgga_latitude();
					last_gps.longitude = nm.gpgga_longitude();
					cout << nm.gpgga_latitude() << endl;
					last_gps.coord_string = to_string(nm.gpgga_latitude()) + " " + to_string(nm.gpgga_longitude()) + "\n";
					last_gps.lock.unlock();
					if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
						GPS_File = fopen(gps_file_name.c_str(), "a");
						fwrite(last_gps.coord_string.c_str(), last_gps.coord_string.length(), 1, GPS_File);
						fclose(GPS_File);
					}
				}
				if (time_sentence == "GGA") {
					last_gps.lock.lock();
					last_gps.time = nm.gpgga_utc();
					uint32_t xx = last_gps.time;
					last_gps.ms = (float)(last_gps.time - xx) * 1000;
					last_gps.ss = xx % 100;
					last_gps.mm = (xx - last_gps.ss) / 100 % 100;
					last_gps.hh = (xx - last_gps.mm - last_gps.ss) / 10000 % 100;
					last_gps.time_string =to_string(last_gps.hh) + ":" + to_string(last_gps.mm) + ":" + to_string(last_gps.ss) + "." + to_string(last_gps.ms) + " ";
					cur_time = (int)nm.gpgga_utc();	//  ���������� ��������� ��������, ����� �� �����������
					last_gps.lock.unlock();
					if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
						GPS_File = fopen(gps_file_name.c_str(), "a");
						fwrite(last_gps.time_string.c_str(), last_gps.time_string.length(), 1, GPS_File);
						fclose(GPS_File);
					}
				}
			}
			break;
		case 'Z':		// GPZDA
			if (time_sentence == "ZDA") {
				last_gps.lock.lock();
				last_gps.time = nm.gpzda_utc();
				uint32_t xx = last_gps.time;
				last_gps.ms = (float)(last_gps.time - xx) * 1000;
				last_gps.ss = xx % 100;
				last_gps.mm = (xx - last_gps.ss) / 100 % 100;
				last_gps.hh = (xx - last_gps.mm - last_gps.ss) / 10000 % 100;
				last_gps.time_string = to_string(last_gps.hh) + ":" + to_string(last_gps.mm) + ":" + to_string(last_gps.ss) + "." + to_string(last_gps.ms) + " ";
				last_gps.lock.unlock();
				cur_time = (int)nm.gpzda_utc();	//  ���������� ��������� ��������, ����� �� �����������
				if (gps_data_save == 1) {		// ���� ���� ���������� ���������� - ���������
					GPS_File = fopen(gps_file_name.c_str(), "a");
					fwrite(last_gps.time_string.c_str(), last_gps.time_string.length(), 1, GPS_File);
					fclose(GPS_File);
				}

			}
			break;
		}
	}
}