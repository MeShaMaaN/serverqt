#include "segd.h"
#include "serial_port2.h"
#include "windows.h"
#include <string>
#include <stdio.h>
#include <cstdint>
#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <ctime>

#include "USB_FTDI_ADC.h"


using namespace std;

extern int16_t buf16_tmp[1000000];		// ����� ��� �������� ����� ������� ����� � ��� � �������� ��������
extern uint32_t counter;	// ��������� ������� ������� ������ ������ ���
extern int32_t buf32[32][25000];		// ����� ��� �������� ����� ������� ����� � ��� � �������� ��������
extern TSamplesArray  buf32_24; // ����� ��� �������� ����� ������� ����� � ���24 � �������� ��������
extern mutex data_lock;	// ���������� ������ ������ - ����� �����, ����� ������ � ��
extern mutex cout_mutex;	// ���������� ������ �� �����
extern int project_shot_number;
extern string project_name;
extern string project_object;
extern string project_customer;
extern string project_performer;
extern string project_ship;
extern string file_prefix;
extern bool file_postfix_enumerator;
extern bool file_postfix_gpstime;
extern string shot_moment;		// ������ �������� �� ��������� �����
extern s_gps last_gps;			// ��������� �������� GPS ������
extern int channel_from;
extern int channel_to;
extern int record_delay;
extern int sample_quantity;	// ���������� ������� �� �����
extern float sample_interval;
extern float record_lenght;
extern int adc14_channels;	// ���������� ������� ���
extern struct tm shot_time;	// ������ �������� �� ���������� �����

SegD::SegD() {		/// �������������, ����������� ��������� �� ���������� ������ �������
	// ������������ ��������� ��������� ��������� GENERAL HEADER  - 32�����.
	file_number = (uint16_t*) gen_header;		// 1-2 � �����(������������� � �������.����)
	format_code = (uint16_t*)gen_header+1;	// 3-4 ��� �������(������������� � �������.����)
	profile_name = (uint16_t*)gen_header + 2;	//	5-6 ��� �������(������������� � �������.����)
	shot_piket = (uint16_t*)gen_header + 3;	// 7-8 ����� ������ ������(������������� � �������.����)
	year = (uint8_t*)gen_header + 10;				//  11 ���
	day2 = (uint8_t*)gen_header + 11;
	day = (uint8_t*)gen_header + 12;				// 13 ����(���������)
	hour = (uint8_t*)gen_header + 13;				// 14 ���
	minute = (uint8_t*)gen_header + 14;			// 15 ������
	second = (uint8_t*)gen_header + 15;			// 16 �������
	manufacter_code = (uint8_t*)gen_header + 16;	// 17 ��� �������������
	base_interval = (uint8_t*)gen_header + 22;		//	23	������� �������� ������
	polarity = (uint8_t*)gen_header + 23;			//	24	����������
	record_type = (uint8_t*)gen_header + 25;		//	26	��� ������
	p_record_lenght = (uint8_t*)gen_header + 26;		//	27	����� ������
	ask_type = (uint8_t*)gen_header + 27;			//	28	��� ������\������
	channel_set = (uint8_t*)gen_header + 28;		//	29	������ �������\��� ������
	mismatch_block = (uint8_t*)gen_header + 29;	//	30	����� ���������������
	extended_header = (uint8_t*)gen_header + 30;	//	31	���� ������������ ���������
	external_header = (uint8_t*)gen_header + 31;	//	32	���� �������� ���������
	memset(gen_header, 0, 32);		/// ������� ��� ���� ������

	
	
									/// 1.	GENERAL HEADER #2 - 32�����.

	ex_file_number = (uint16_t*) (gen_header2+1);		// 1-2-3 � �����(������������� � �������.����)
	extended_blocks = (uint16_t*)(gen_header2+5) ;	// 6-7 ����� ������������ ��������� � 32 ������ ������
	external_blocks = (uint16_t*)(gen_header2 + 7);	// 8-9 ����� ������������ ��������� � 32 ������ ������
	rev1 = (uint8_t *)gen_header2 + 10;				//  11 ������ ������� segD
	rev2 = (uint8_t *)gen_header2 + 11;				// 12
	ext_record_lenght = (uint16_t *)(gen_header2 + 15);				// 15-16-17 Extended Record Length ms 
	gen_header_number = (uint8_t *)gen_header2 + 18;				// 19	General Header Block Number
	memset(gen_header2, 0, 32);		/// ������� ��� ���� ������		// ������ ��� ��������� ���������

									
									////	SCAN TYPE HEADER.CHANEL SET 1 - 32�����.
	
	 q_type = (uint8_t *)scan_header;			//	1	� ���� ������
	channel_set_num = (uint8_t *)(scan_header+1);	//	2	� ������ �������
	begining_time = (uint16_t*)(scan_header+2);	//	3 - 4	 ����� ������ ������ �������
	ending_time = (uint16_t*)(scan_header+4);	// 5 - 6	����� ����� ������ �������
	mr_factor = (uint16_t*)(scan_header+6);		// 7 - 8	MP ������
	channels_quantity = (uint16_t*)(scan_header+8);	//9 - 10	 ����� �������(� ���������� �������������)
	channel_type = (uint8_t *)(scan_header+10);			//	11	��� ������
	channel_gain = (uint8_t *)(scan_header+11);			//	12	�������� ������
	antialiasing_frqns = (uint16_t*)(scan_header+12);	//	13 - 14	������� ����������������� �������(� ���������� �������������)
	antialiasing_slope = (uint16_t*)(scan_header+14);	// 15 - 16	�������� ����������������� �������(� ���������� �������������)
	h_freq_filter = (uint16_t*)(scan_header+16);		// 17 - 18	���(� ���������� �������������)
	h_freq_filter_slope = (uint16_t*)(scan_header+18);	// 19 - 20	�������� ���(� ���������� �������������)
	memset(scan_header, 0, 32);		/// ������� ��� ���� ������

	///	LINE DESCRIPTOR HEADER - 32�����.
	
	file_number_ex =(uint16_t*)line_header;	/// 1 - 3	����������� ����� �����
	profile_number = (uint16_t*)(line_header+3);	///	4 - 6	����� �������
	stirring_piket = (uint16_t*)(line_header+8);	//	9 - 11	����� ������ ����������� !!!!!!!!!!!!!!!!����� ��� 3 �����
	stirrer_set = (uint8_t *)(line_header+19);			//	20	����� ������ ����������
	memset(line_header, 0, 32);		/// ������� ��� ���� ������

	/// TRACE HEADER - 20 ����.
	trace_file_number = (uint16_t*)trace_header;				/// 1 - 2	����� ����� (���� ��� �����)
	inter_type_number = (uint8_t *)(trace_header +2);			//	3	����� ���� ������
	trace_channel_set_num = (uint8_t*)(trace_header + 3);			//	4	����� ������ �������
	track_number = (uint16_t*)(trace_header+4);			//	5 - 6	����� ������
	sample_skew=(uint8_t*)(trace_header + 10);		// 11 fractional part of the base scan interval
	ex_trace_file_number = (uint16_t*)(trace_header + 18);	// ����� ��������/�����
	memset(trace_header, 0, 20);		/// ������� ��� ���� ������
	
}

void SegD::WriteSegDfrom14(void)
{
	HANDLE   hFile = NULL, hMap = NULL, hFile1 = NULL, hMap1 = NULL;
	void     *fdata = NULL, *fdata1 = NULL;
	long     dsize;		// ����� ������ � ������������ �������
	long     hsize;		// ������ ��������� segD
	long     fsize;		// �������� ����������� (������ + ���������)
	void    *data1;		// ��������� �� ������ � ���
	ULONG   *sync1;
	clock_t timeX;
	void *tmp, *tmp1, *tmp3;
	string str;
	string file_name =  "c:/segd/" ;
	file_name = file_name + project_name + "/";
    CreateDirectory((LPCWSTR)file_name.c_str(), NULL);	// ������ ����� ��� segd ������
	DatasArray * dataStruct = new  DatasArray;
	float temp_data[25000];
	int32_t i_temp_data[25000];
	timeX = clock();	// �������� ������ ������ ������

	file_name += file_prefix;
	//if (file_postfix_enumerator)
	str = to_string(project_shot_number);
	for (int i = 0; i < (6-str.length()); i++ )
		file_name += "0";
	file_name += str;
	if (file_postfix_gpstime)
		file_name += shot_moment;
		file_name += ".segd";
	fsize = 96+ EXT_HEADER_SIZE + EXTD_HEADER_SIZE +(channel_to-channel_from+1)*(sample_quantity*4+20); // ������ �����
    hFile = CreateFile((LPCWSTR)file_name.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_FLAG_RANDOM_ACCESS, NULL);
	if (INVALID_HANDLE_VALUE == hFile) {
		cout << "������ �������� �����: " << file_name <<endl;
	}
	// ������ ��������� � ����
	//WriteFile(hFile, Buffer, 17, NULL, NULL);
	hMap = CreateFileMapping(hFile, NULL, PAGE_READWRITE, 0, fsize, NULL);
	fdata = MapViewOfFile(hMap, FILE_MAP_WRITE, 0, 0, 0);
	
	///GENERAL HEADER #1
	tmp = (char *)fdata;                     // ����������� ��������� � �����
	tmp1 = gen_header;                   // ����������� ���������
    memcpy(tmp, tmp1, size_t(gen_header));   // ���������� ������ � ����
	///GENERAL HEADER #2
	tmp = (char *)fdata+32;                     // ����������� ��������� � �����
	tmp1 = gen_header2;                   // ����������� ��������� 
    memcpy(tmp, tmp1, size_t(gen_header2));   // ���������� ������ � ����

	///SCAN TYPE HEADER.CHANEL SET 1 - 32�����.
	tmp = (char *)fdata + 64;                     // ����������� ��������� � �����
	tmp1 = scan_header;                   // ����������� ���������
	memcpy(tmp, tmp1, 32);   // ���������� ������ � ����

	/// Extended HEADER ��������� ������, �������� ����������, �����, ����, �������, GPS
	tmp = (char *)fdata + 96;                     // ����������� ��������� � �����
	for (int i = 0; i < project_name.length(); i++)
		v[i] = project_name[i];
	for (int i = 0; i < project_object.length(); i++)
		v[i+64] = project_object[i];
	for (int i = 0; i < project_customer.length(); i++)
		v[i + 128] = project_customer[i];
	for (int i = 0; i < project_performer.length(); i++)
		v[i + 192] = project_performer[i];
	for (int i = 0; i < project_ship.length(); i++)
		v[i + 256] = project_ship[i];
	for (int i = 0; i < last_gps.coord_string.length(); i++)
		v[i + 320] = last_gps.coord_string[i];
	for (int i = 0; i < last_gps.time_string.length(); i++)
		v[i + 384] = last_gps.time_string[i];

	extern string file_prefix;
	extern bool file_postfix_enumerator;
	extern bool file_postfix_gpstime;
	
	
	tmp = (char *)fdata + 96;

	tmp1 = v;                   // ����������� ��������� 
	//memcpy(tmp, tmp1, EXTD_HEADER_SIZE);   // ���������� ������ � ����
	memcpy(tmp, tmp1, sizeof(v));   // ���������� ������ � ����

	// ��������� ���� ������
	float zero[32];
	for (int i =0; i < adc14_channels; i++) {
		///   ������� TRACE HEADER
		*track_number = IntToHexDec(i);
		tmp = (char *)fdata + 96+ EXT_HEADER_SIZE + EXTD_HEADER_SIZE +i*(sample_quantity*4+20);    // ����������� ���������
		tmp1 = trace_header;
		memcpy(tmp, tmp1, 20);   // ���������� ������ � ����
		float *pData_f;
		zero[i] = 0;
		int min = 10000;
		int max = -10000;
		int temp;
		/*for (uint32_t ii = 0; ii < 1000; ii++)
		{
			temp = buf16_tmp[ii * adc14_channels + i];
			if (temp > max) max = temp;
			if (temp < min) min = temp;
			zero[i] += temp / 256;		// ������� �������
		}
		cout << "Min " << min << endl;
		cout << "Max " << max << endl;*/
		for (uint32_t ii = 0; ii < sample_quantity; ii++)
		{
			//temp_data[ii] =((float)sin(ii / 100.1) * 6000000 * (sin((ii*i) / 1000.0)*ii / sample_quantity));
			temp_data[ii] =buf16_tmp[ii * adc14_channels + i];		// 14 ������ ���
			//i_temp_data[ii] = htons(buf16_tmp[ii * adc14_channels + i]);		// 14 ������ ���
			FloatToIEEE(&temp_data[ii]);
			
		}

		/// ������ ������ ������ ������
		tmp = (char *)fdata + 96 + EXT_HEADER_SIZE + EXTD_HEADER_SIZE + i*(sample_quantity * 4 + 20)+20;                     // ����������� ��������� � �����
		tmp1 = (char *)temp_data;                   // ����������� ��������� � ���������
		
		memcpy(tmp, tmp1, sample_quantity * 4);   // ���������� ������ � ����
	}



	if (fdata) UnmapViewOfFile(fdata);
	if (hMap) CloseHandle(hMap);
	if (hFile) CloseHandle(hFile);
	delete (dataStruct);
	std::this_thread::sleep_for(std::chrono::milliseconds(10));	/// �����, ����� �� ������ ��
	//cout_mutex.lock();
	//cout << "������ � segD: " << (float)(clock() - timeX) / CLOCKS_PER_SEC << endl;
	//cout_mutex.unlock();
}

void SegD::Init() {		/// �������������, ����������� ��������� �� ���������� ������ �������
	// ������������ ��������� ��������� ��������� GENERAL HEADER #1 - 32�����.
	*file_number = 0xFFFF;// IntToHexDec(2345);		// 1-2 � �����(������������� � �������.����)
	*format_code = 0x5880;	// 3-4 ��� �������(������������� � �������.����) 32 bit IEEE (float C++)
	//*format_code = 0x3880;
	//profile_name = (uint16_t*)gen_header + 4;	//	5-6 ��� �������(������������� � �������.����)
	//shot_piket = (uint16_t*)gen_header + 6;	// 7-8 ����� ������ ������(������������� � �������.����)
	char buf[80];
	char year_now[10];
	char month_now[10];
	char day_now[10];
	char hour_now[10];
	char minute_now[10];
	char second_now[10];
	strftime(year_now, sizeof(year_now), "%y", &shot_time);
	strftime(month_now, sizeof(month_now), "%m", &shot_time);
	strftime(day_now, sizeof(day_now), "%j", &shot_time);
	strftime(hour_now, sizeof(hour_now), "%H", &shot_time);
	strftime(minute_now, sizeof(minute_now), "%M", &shot_time);
	strftime(second_now, sizeof(second_now), "%S", &shot_time);
	


	*year = CharToHexDec(stoi(year_now));				//  11 ���
	*day2 = Collect2Char(1, IntToCharHigh(stoi(day_now)));						// 12 ���������� ��� ������ + ������� 4 ���� ���
	*day = IntToCharLow(stoi(day_now));				// 13 ����(���������)
	clock_t timeX;

	*hour = CharToHexDec(stoi(hour_now));			// 14 ���
	*minute = CharToHexDec(stoi(minute_now));			// 15 ������
	*second = CharToHexDec(stoi(second_now));			// 16 �������
	*manufacter_code = CharToHexDec(99);	// 17-19 ��� �������������
	
	int x = 1.0 / sample_interval;
	switch (x)
	{
	case 32:
		*base_interval = 0;		//	23	������� �������� ������
		break;
	case 16:
		*base_interval = 1;		//	23	������� �������� ������
		break;
	case 8:
		*base_interval = 2;		//	23	������� �������� ������
		break;
	case 4:
		*base_interval = 4;		//	23	������� �������� ������
		break;
	case 2:
		*base_interval = 8;		//	23	������� �������� ������
		break;
	case 1:
		*base_interval = 16;		//	23	������� �������� ������
		break;
	}
	*polarity = 0;			//	24	����������
	*record_type = 128 | 0x0f;		//	26	��� ������ ���������� ���������� � ������� ������ + 4 ���� ����� ������
	*p_record_lenght = 255;		//	27	����� ������ ����� ������ � ���� ���������
	*ask_type = 1;			//	28	��� ������\������
	*channel_set = 1;		//	29	������ �������\��� ������
	*mismatch_block = 0;	//	30	����� ���������������
	*extended_header = CharToHexDec(EXTD_HEADER_SIZE / 32);	//	31	���� ������������ ���������
	*external_header = 0;	//	32	���� �������� ���������
	
							/// 1.	GENERAL HEADER #2 - 32�����.
	
	* ex_file_number = IntToHexDec(project_shot_number);		// 1-2-3 � �����(������������� � �������.����)
	*extended_blocks = htons(0);	// 6-7 ����� ������������ ��������� � 32 ������ ������
	*external_blocks = htons(0);	// 6-7 ����� ������������ ��������� � 32 ������ ������
	* rev1 = 2;				//  11 ������ ������� segD
	* rev2 = 0;				// 12
	x = (int)record_lenght;
	* ext_record_lenght = htons(x);				// 15-16-17 Extended Record Length ms 
	* gen_header_number = 2;				// 19	General Header Block Number
	

							////	SCAN TYPE HEADER.CHANEL SET 1 - 32�����.

	*q_type = 1;			//	1	� ���� ������
	*channel_set_num = 1;	//	2	� ������ �������	
	*begining_time =  htons(record_delay/2);	//	3 - 4	 ����� ������ ������ �������
	*ending_time = htons(((int)record_lenght + record_delay)/2);	// 5 - 6	����� ����� ������ �������
	*mr_factor = 0;		// 7 - 8	MP ������
	*channels_quantity = IntToHexDec(adc14_channels);	//9 - 10	 ����� �������(� ���������� �������������)
	*channel_type = Collect2Char(1,0);			//	11	��� ������
	*channel_gain = Collect2Char(0,0);			//	12	Number of samples exponent, Channel gain control method
	*antialiasing_frqns = 0;	//	13 - 14	������� ����������������� �������(� ���������� �������������)
	*antialiasing_slope = 0;	// 15 - 16	�������� ����������������� �������(� ���������� �������������)
	*h_freq_filter = 0;		// 17 - 18	���(� ���������� �������������)
	*h_freq_filter_slope = 0;	// 19 - 20	�������� ���(� ���������� �������������)

	/// Extended HEADER
	//char xchar[80]= "more Spokoistvia, kosmolet TOVARISCH 13:23:45 2016.12.23 N46.569321 W56.348723 ";
	//extended_header_string = "more Spokoistvia, kosmolet TOVARISCH 13:23:45 2016.12.23 N46.569321 W56.348723 ";
	memset(extended_header_string, 0x00, EXTD_HEADER_SIZE);	// ��������� ��������� ����� AA
	//memcpy(extended_header_string, xchar, 80);
	/*
									///	LINE DESCRIPTOR HEADER - 32�����.

	file_number_ex = (uint16_t*)line_header;	/// 1 - 3	����������� ����� �����
	profile_number = (uint16_t*)line_header + 3;	///	4 - 6	����� �������
	stirring_piket = (uint16_t*)line_header + 8;	//	9 - 11	����� ������ ����������� !!!!!!!!!!!!!!!!����� ��� 3 �����
	stirrer_set = (uint8_t *)line_header + 19;			//	20	����� ������ ����������
	memset(line_header, 0, 32);		/// ������� ��� ���� ������
	*/
									/// TRACE HEADER - 20����.
	
	*trace_file_number = 0xFFFF;				/// 1 - 2	����� ����� 
	*inter_type_number = 1;			//	3	����� ���� ������
	*trace_channel_set_num = 1;			//	4	����� ������ ������� (���� ��� �����)
	*track_number = IntToHexDec(1);			//	5 - 6	����� ������
	*sample_skew = 0;	
	*ex_trace_file_number = IntToHexDec(project_shot_number); //Extended File Number : 5425
}
/// ���������� uint8 �������� 0-99 ���������� � ���������� inthex � ��������� 0x00 - 0x99 
uint8_t SegD::CharToHexDec(uint8_t input) {
	uint8_t a = input % 10;		// �������� ������� ������
	uint8_t b = (input - a)/10;	// �������� ������� ������
	uint8_t c = (b << 4) + a;	// ��������� �������
	return c;
}
/// ���������� uint16 �������� 0-9999 ����� ������� 2 ������� � ������ inthex  � ��������� 0x00 - 0x99 
uint8_t SegD::IntToCharLow(uint16_t input) {
	uint16_t a = input % 100;	// �������� 2 ������� ������� (0-99)
	uint16_t b = a % 10;		// �������� ������� ������
	uint16_t c = (a - b) / 10;	// �������� ������� ������
	uint8_t d = (c << 4) + b;	// ��������� �������
	return d;
}
/// ���������� uint16 �������� 0-9999 ����� ������� 2 ������� � ������ hex  � ��������� 0x00 - 0x99 
uint8_t SegD::IntToCharHigh(uint16_t input) {
	uint16_t a = input % 100;	// �������� 2 ������� ������� (0-99)
	uint16_t b = (input - a) / 100;	// �������� ������� �� ����� �������
	uint16_t c = b % 10;		// �������� ������� ������
	uint16_t d = (b - c) / 10;	// �������� ������� ������
	uint8_t e = (d << 4) + c;	// ��������� �������
	return e;
}
/// ����� 2 uint8 ���������� hex ��������� 0x0 - 0x09 � ���������� � ���������� inthex 0x0 - 0x99
uint8_t SegD::Collect2Char(uint8_t inputH, uint8_t inputL) {
	uint8_t a = (inputH & 0x0f) << 4;	// �������� 2 ������� ������� (0-99), �������� � ������� ��������
	uint8_t b = (inputL & 0x0f) + a;	// ���������
	return b;
}
/// ���������� uint16 �������� 0-9999 � ������ inthex  � ��������� 0x00 - 0x9999 
uint16_t SegD::IntToHexDec(uint16_t input) {
	uint16_t a = input % 100;	// �������� 2 ������� ������� (0-99)
	uint16_t b = a % 10;		// �������� ������� ������
	uint16_t c = (a - b) / 10;	// �������� ������� ������
	uint8_t h = (c << 4) + b;	// ��������� �������  - �������� ������� ������
	
	b = (input - a) / 100;	// �������� ������� �� ����� �������
	c = b % 10;		// �������� ������� ������
	uint16_t d = (b - c) / 10;	// �������� ������� ������
	uint8_t l = (d << 4) + c;	// ��������� �������  - �������� ������� ������

	a = (h << 8) + l;	// �������� 16 ������ �����
	return a;
}
/// ������ ������������������ ���� � float ���� ��� ������ � segD
void FloatToIEEE(float * input) {
	char * x0 = (char*)input;
	char * x1 = (char*)input+1;
	char * x2 = (char*)input+2;
	char * x3 = (char*)input+3;
	char a = *x0;
	char b = *x1;
	char c = *x2;
	char d = *x3;
	*x0 = d;
	*x1 = c;
	*x2 = b;
	*x3 = a;
}
