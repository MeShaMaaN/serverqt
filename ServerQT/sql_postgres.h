#pragma once
#include <stdio.h>
#include <iomanip>
#include <iostream>   // std::cin, std::cout
#include <string>     // std::string, std::stoul, std::getline
#include <conio.h>

#define DB_PATH "dbname=SborEx port=5433 host=localhost user=postgres password=12345"

void WriteADCtoSQLfake(int);
void WriteADCtoSQLint(void);
int ReadGPSconfig(void);
int ReadConfigfromSQL(void);
int WaitForStart(void);
void ClearBase(void);
int ReadConfigFile();