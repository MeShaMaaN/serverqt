#include <libpq-fe.h>
#include <stdio.h>
#include <iomanip>
#include <iostream>   // std::cin, std::cout
#include <string>     // std::string, std::stoul, std::getline
#include "sql_postgres.h"
#include <array>
#include <fstream>
#include <sstream>
#include <process.h> 
#include "serial_port2.h"
#include "segd.h"
#include <chrono>
#include <thread>
#include <mutex>
#include <atomic>
#include "USB_FTDI_ADC.h"

/* for ntohl/htonl */
//#include <netinet/in.h>
//#include <arpa/inet.h>
//#include <WinSock2.h>


extern int16_t buf16[1000000];		// ����� ��� �������� ����� ������� ����� � 14 ������ ��� � �������� ��������
extern int16_t buf16_tmp[1000000];		//��������� ����� ��� �������� ����� ������� ����� � ��� � �������� ��������
extern uint32_t counter;	// ��������� ������� ������� ������ ������ ���
extern int32_t buf32[32][25000];		// ����� ��� �������� ����� ������� ����� � ��� � �������� ��������
extern TSamplesArray  buf32_24; // ����� ��� �������� ����� ������� ����� � ���24 � �������� ��������
extern double adc14_freq;	// ������� ���
extern int adc14_pages;		// ���������� ������� ������ ��� �� 6144 
extern int adc_cycles;		// ���������� �������� ��� ���������� ������
extern int adc14_channels;	// ���������� ������� ���
extern bool adc_diff_input;	// ����� ������ ��� - ����������������
using namespace std;

extern string gps_port;	// ���� ��� ����������� GPS
extern string gps_file_name;	// 
extern string nmea_file_name;	// 
extern int port_speed;
extern int gps_data_save;
extern int nmea_data_save;
extern bool gps_on;	// ����� �� �������� gps
extern bool depth_on;	//����� �� �������� ������ �������

extern int adc_type;
extern int channel_from;
extern int channel_to;
extern int shot_period;
extern float sample_interval;
extern float record_lenght;
extern int record_delay;
extern int depth_port;
extern atomic<bool> is_running;	// ������� �� ������ �����
extern int project_shot_number;	// ����� �������� � �������� �������� �������
extern int sample_quantity;	// ���������� ������� �� �����
extern int streamer_gain;		// �����. �������� ��� ��� ���������� ����
extern atomic <bool> data_ready;	// ���� ���������� ������ ��� ��� ������ � ��	
extern atomic <bool> connect_DB;	// ���� ������� ����������� � ��
extern mutex data_lock;
extern mutex cout_mutex;	// ���������� ������ �� �������

extern string project_name;
extern string project_object;
extern string project_customer;
extern string project_performer;
extern string project_ship;
extern string file_prefix;
extern bool file_postfix_enumerator;
extern bool file_postfix_gpstime;
extern bool internal_syncro;		// ����� ������������� �� ����������� �������

extern s_gps last_gps;			// ��������� �������� GPS ������
s_gps old_gps;			// ���������� �������� GPS ������
extern string time_sentence;	// �������� GPS ����� (RMC, GGA, ZDA, VTG, GLL)
extern string coord_sentence;
extern string course_sentence;

extern struct tm shot_time;	// ������ �������� �� ���������� �����

int WriteToLog(std::string message);


void WriteADCtoSQLint() {		// ������ �������� ��� � SQL
	//_endthread();
	PGconn          *conn;
	PGresult        *res;
	char *paramValues[50];
	int paramLenghts[50];
	int paramFormats[50];
	int plate;
	int chnl;
	clock_t timeX;
	SegD * segd = new SegD();
	int temp[10];	// ��������� ���. ���������� ��� ������ � ��
	void *tmp1;
	void *tmp3;
	int16_t ** adc_data = new int16_t*[32];		// ������ �������� ������ � ��
	for (int i = 0; i < 32; i++) {
		adc_data[i] = new int16_t[25000];
	}
	uint64_t zero[32];	// �������� ���� ��� ������� ������
	cout_mutex.lock();
	cout << "������� ����� ������ � �� " << this_thread::get_id() << endl;
	cout_mutex.unlock();

	conn = PQconnectdb(DB_PATH);
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout_mutex.lock();
		cout << "�� ������� ������������ � ���� " << DB_PATH << endl;
		cout_mutex.unlock();
		goto end_fast;	// ������� �� ������ ����������� � ����, �.�. ��� �� ���������
	}
	else
	{
		cout_mutex.lock();
		cout << "�������� ����������� � ��" << DB_PATH << endl;
		cout_mutex.unlock();
	}

	while (connect_DB)
	{
		cout << "����� ������ �� ������� ������..." << endl;
		if (data_ready)		// �� ������ ��������� ������ ������ ��������, � ��� ��� ���� ������!
			cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! �� �������� ���������� �����!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;

		while (!data_ready)// �������� ��������� ����� ������
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		timeX = clock();	// �������� ������ ������ ������
		if (!connect_DB)
			goto end_thread;
		// ��������� ������ ����������
		segd->Init();				//// �������������� segD �����
		switch (adc_type)
		{
		case 14:
			cout_mutex.lock();
			cout << "�������� ������ � 14 ������� ���" << endl;
			cout_mutex.unlock();

			tmp1 = (char *)buf16_tmp;
			tmp3 = (char*)buf16;                   // ����������� ��������� ����� ������ ������
			data_lock.lock();			// ��������� ������ � ������ �������
			memcpy(tmp1, tmp3, adc14_channels* sample_quantity * 2);   // 
			data_lock.unlock();

			segd->WriteSegDfrom14();			//// ������ segD 
			cout << "������ SEG-D ������������: " << (float)(clock() - timeX) / CLOCKS_PER_SEC << endl << endl;
			//cout_mutex.lock();
			//cout << "���������� �����" << endl;
			//cout_mutex.unlock();
			for (int xx = 0; xx < 32; xx++) {
				// ������ ����
				zero[xx] = 0;
				for (uint32_t ii = 0; ii < 256; ii++)
				{
					zero[xx] += buf16_tmp[ii * adc14_channels + xx] / 256;		// ������� �������
				}
				// ���������� ������� ��� ��������
				for (uint32_t ii = 0; ii < sample_quantity; ii++)
				{
					adc_data[xx][ii] = zero[xx] - buf16_tmp[ii * adc14_channels + xx];		// 14 ������ ���
				}
				paramValues[xx] = (char*)adc_data[xx];
				if (xx < adc14_channels)
					paramLenghts[xx] = sample_quantity * 2;		/// ����� � ������ ������� ������
				else
					paramLenghts[xx] = 0;	// �� ������ ������ adc14_channels ������� ��� �������� ������ � ��
				paramFormats[xx] = 1;
			}
			break;
		case 24:
			cout_mutex.lock();
			cout << "�������� ������ � 24 ������� ���" << endl;
			cout_mutex.unlock();
			for (int i = channel_from; i < channel_to; i++) {
				buf32_24.Mutex.lock();			// ��������� ������ � ������ �������
				// ������ ����
				zero[i] = 0;
				for (uint32_t ii = 0; ii < 256; ii++)
				{
					if (i <= 16) {
						plate = 0;
						chnl = i;
					}
					else {
						plate = 1;
						chnl = i - 16;
					}
					zero[i] += buf32_24.Sample[plate][chnl][ii] / 256;		// ������� �������
				}
				for (uint32_t ii = 0; ii < sample_quantity; ii++)
				{
					adc_data[i][ii] = (zero[i] - buf32_24.Sample[plate][chnl][ii]) << 8; // �������� �� 16 ��� ��
				}
				buf32_24.Mutex.unlock();;			// ������������
				paramValues[i] = (char*)adc_data[i];
				if (i < 16)
					paramLenghts[i] = sample_quantity * 2;		/// ����� � ������ ������� ������
				else
					paramLenghts[i] = 0;	// �� ������ ���� ������ 19 ������� ��� �������� ������ � ��
				paramFormats[i] = 1;
			}
			break;
		}

		data_ready = false;		// ������� ���� - ������ �� �������
		cout_mutex.lock();			// ��������� ������ � ������ �������
		cout << "������������� ������ ��� ������ � ��: " << (float)(clock() - timeX) / CLOCKS_PER_SEC << endl;
		cout_mutex.unlock();

		// ����� ������
		temp[0] = htonl(project_shot_number);		// �������������� ����� ��� ��
		paramValues[32] = (char*)&temp[0];
		paramLenghts[32] = 4;		/// ����� � ������
		paramFormats[32] = 1;
		// ���������� �������� � ������
		temp[2] = htonl(sample_quantity);		// �������������� ����� ��� ��
		paramValues[33] = (char*)&temp[2];
		paramLenghts[33] = 4;		/// ����� � ������
		paramFormats[33] = 1;
		// ������ ������������� � ��
		float f_temp = sample_interval;
		FloatToIEEE(&f_temp);
		paramValues[34] = (char*)&f_temp;
		paramLenghts[34] = 4;		/// ����� � ������
		paramFormats[34] = 1;
		// ���������� �������
		temp[3] = htonl(adc14_channels);
		paramValues[35] = (char*)&temp[3];
		paramLenghts[35] = 4;		/// ����� � ������
		paramFormats[35] = 1;

		int resultFormat = 0; /* text */
		int nParams = 36;
		res = PQexecParams(conn,
			"insert into public.adc_data (shot_time, chanel_quantity, sample_interval, sample_quantity, shot_id, data_01, data_02, data_03, data_04, data_05, data_06, data_07, data_08, data_09, data_10, data_11, data_12, data_13, data_14, data_15, data_16, data_17, data_18, data_19, data_20, data_21, data_22, data_23, data_24, data_25, data_26, data_27, data_28, data_29, data_30, data_31, data_32) values ('now', $36::integer, $35::real, $34::integer, $33::integer, $1::bytea, $2::bytea, $3::bytea, $4::bytea, $5::bytea, $6::bytea, $7::bytea, $8::bytea, $9::bytea, $10::bytea, $11::bytea, $12::bytea, $13::bytea, $14::bytea, $15::bytea, $16::bytea, $17::bytea, $18::bytea, $19::bytea, $20::bytea, $21::bytea, $22::bytea, $23::bytea, $24::bytea, $25::bytea, $26::bytea, $27::bytea, $28::bytea, $29::bytea, $30::bytea, $31::bytea, $32::bytea)",
			nParams,
			NULL,
			paramValues,
			paramLenghts,
			paramFormats,
			resultFormat
			);

		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			cout_mutex.lock();
			cout << "������ ������ � �� adc_data" << PQerrorMessage(conn) << endl;
			cout_mutex.unlock();
			WriteToLog("Database Writing error");
		}
		else
		{
			cout_mutex.lock();
			//cout << "������ ������ adc_data" << endl;
			cout_mutex.unlock();
		}
		/// ��������� ��������� ����� �������� � �������
		temp[0] = htonl(project_shot_number);		// �������������� ����� ��� ��
		paramValues[0] = (char*)&temp[0];
		paramLenghts[0] = 4;		/// ����� � ������
		paramFormats[0] = 1;
		nParams = 1;
		res = PQexecParams(conn,
			"UPDATE public.settings SET last_shot_id = $1::integer where is_active = true",
			nParams,
			NULL,
			paramValues,
			paramLenghts,
			paramFormats,
			resultFormat
			);

		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			cout_mutex.lock();
			cout << "������ ������ � �� settings" << PQerrorMessage(conn) << endl;
			cout_mutex.unlock();
			WriteToLog("Database Writing error");
		}
		else {
			cout_mutex.lock();
			//cout << "������ ������ settings" << endl;
			cout_mutex.unlock();
		}
		cout_mutex.lock();
		cout << "������ � �� �������: " << (float)(clock() - timeX) / CLOCKS_PER_SEC << endl;
		cout_mutex.unlock();
		res = PQexec(conn, "DELETE FROM public.adc_data WHERE id IN (SELECT id FROM public.adc_data ORDER BY id DESC OFFSET 1000)");
		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			cout_mutex.lock();
			cout << "������ ������� ����!!!!" << endl;
			cout_mutex.unlock();
		}
		
	}
end_thread:
	PQclear(res);
	PQfinish(conn);
end_fast:
	for (int i = 0; i < 32; i++) {
		delete[] adc_data[i];
	}
	delete[] adc_data;
	cout_mutex.lock();
	cout << "����� ������ �� ��������!" << endl;
	cout_mutex.unlock();
	_endthread();	// ��������� ���� �����
}

void WriteADCtoSQLfake(int delta) {		// �������� ������ �������� ��� � SQL 
	PGconn          *conn;
	PGresult        *res;
	char *paramValues[50];
	int paramLenghts[50];
	int paramFormats[50];

	/// ���������� ����� �� ������ ��������
	auto now_time = time(0);
	shot_time = *localtime(&now_time);


	SegD * segd = new SegD();

	int16_t ** adc_data = new int16_t*[32];		// ������ �������������� INT-HEX
	for (int i = 0; i < 32; i++) {
		adc_data[i] = new int16_t[25000];
	}
	conn = PQconnectdb(DB_PATH);
	//conn = PQconnectdb("dbname=SborEx port=5433 host=81.95.140.154 user=postgres password=12345");
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout << "�� ������� ������������ � ����" << endl;
	}

	for (uint32_t i = 0; i < sample_quantity; i++) {
		for (int ii = 0; ii < adc14_channels ; ii++)
		{
			
			buf32[ii][i] = (float)sin(i / 100.1) * 6000.1 * abs(sin(ii*i / 1000.1 + delta))*((float)i/sample_quantity);
			//buf32[ii][i] = (float)sin(i / 100.1 + delta) * 6000.1;
			buf16[ii+i*adc14_channels] = buf32[ii][i];
			adc_data[ii][i] = buf32[ii][i];
		}		
	}
	for (int ii = 0; ii < 32; ii++)
	{
		paramValues[ii] = (char*)adc_data[ii];
		if (ii < adc14_channels)
			paramLenghts[ii] = sample_quantity * 2;		/// ����� � ������ ������� ������
		else
			paramLenghts[ii] = 0;	// �� ������ ���� ������ 16 ������� ��� �������� ������ � ��
		paramFormats[ii] = 1;
	}
	// ��������� ������ �� ��������� ����� ��� ������������ ������������ segD
	char * tmp1 = (char *)buf16_tmp;
	char * tmp3 = (char*)buf16;                   // ����������� ��������� ����� ������ ������
	data_lock.lock();			// ��������� ������ � ������ �������
	memcpy(tmp1, tmp3, adc14_channels * sample_quantity * 2);   // 
	data_lock.unlock();

	segd->Init();				//// �������������� segD �����
	segd->WriteSegDfrom14();			//// ������ segD 

	cout << "��������� �����" << endl;
	uint32_t temp[10];
	// ����� ������
	project_shot_number++;
	temp[0] = htonl(project_shot_number);		// �������������� ����� ��� ��
	paramValues[32] = (char*)&temp[0];
	paramLenghts[32] = 4;		/// ����� � ������
	paramFormats[32] = 1;
	// ���������� �������� � ������
	temp[2] = htonl(sample_quantity);		// �������������� ����� ��� ��
	paramValues[33] = (char*)&temp[2];
	paramLenghts[33] = 4;		/// ����� � ������
	paramFormats[33] = 1;
	// ������ ������������� � ��
	float f_temp = sample_interval;
	FloatToIEEE(&f_temp);
	paramValues[34] = (char*)&f_temp;
	paramLenghts[34] = 4;		/// ����� � ������
	paramFormats[34] = 1;
	// ���������� �������
	temp[3] = htonl(channel_to - channel_from + 1);
	paramValues[35] = (char*)&temp[3];
	paramLenghts[35] = 4;		/// ����� � ������
	paramFormats[35] = 1;



	int resultFormat = 0; /* text */
	int nParams = 36;
	res = PQexecParams(conn,
		"insert into public.adc_data (shot_time, chanel_quantity, sample_interval, sample_quantity, shot_id, data_01, data_02, data_03, data_04, data_05, data_06, data_07, data_08, data_09, data_10, data_11, data_12, data_13, data_14, data_15, data_16, data_17, data_18, data_19, data_20, data_21, data_22, data_23, data_24, data_25, data_26, data_27, data_28, data_29, data_30, data_31, data_32) values ('now', $36::integer, $35::real, $34::integer, $33::integer, $1::bytea, $2::bytea, $3::bytea, $4::bytea, $5::bytea, $6::bytea, $7::bytea, $8::bytea, $9::bytea, $10::bytea, $11::bytea, $12::bytea, $13::bytea, $14::bytea, $15::bytea, $16::bytea, $17::bytea, $18::bytea, $19::bytea, $20::bytea, $21::bytea, $22::bytea, $23::bytea, $24::bytea, $25::bytea, $26::bytea, $27::bytea, $28::bytea, $29::bytea, $30::bytea, $31::bytea, $32::bytea)",
		nParams,
		NULL,
		paramValues,
		paramLenghts,
		paramFormats,
		resultFormat
		);

	if (!res) {
		cout << "RES IS EMPTY!" << endl;
		return;
	}

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		cout << "������ ������ � ��" << PQerrorMessage(conn) << endl;
		WriteToLog("Database Writing error");
	}
	else
		cout << "������ ������" << endl;

/// ��������� ��������� ����� �������� � �������
	temp[0] = htonl(project_shot_number);		// �������������� ����� ��� ��
	paramValues[0] = (char*)&temp[0];
	paramLenghts[0] = 4;		/// ����� � ������
	paramFormats[0] = 1;

	nParams = 1;
	res = PQexecParams(conn,
		"UPDATE public.settings SET last_shot_id = $1::integer where is_active = true",
		nParams,
		NULL, /* Types of parameters, unused as casts will define types */
		paramValues,
		paramLenghts,
		paramFormats,
		resultFormat
		);

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		cout << "������ ������ � ��" << PQerrorMessage(conn) << endl;
		WriteToLog("Database Writing error");
	}
	else
		cout << "������ ������" << endl;

	PQclear(res);
	PQfinish(conn);

	for (int i = 0; i < 32; i++) {
		delete[] adc_data[i];
	}
	delete[] adc_data;
}

int ReadGPSconfig(void) {
	clock_t timeX = clock();	// ������� �����
	PGconn          *conn;
	PGresult        *res;
	int             rec_count;
	int             row;
	int             col;
	int	i_fnum;
	int32_t ival[3];
	char       *iptr;
	conn = PQconnectdb(DB_PATH);
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout << "�� ������� ������������ � ����" << endl;
		WriteToLog("Missing database");
	}
	//while (connect_DB == true)
	{
		///  �������� ���� ������
		res = PQexec(conn, "SELECT * FROM public.gps_settings ORDER BY id DESC LIMIT 1");
		if (PQresultStatus(res) != PGRES_TUPLES_OK)
		{
			cout_mutex.lock();
			cout << "������ ��������� ������������ GPS" << endl;
			cout_mutex.unlock();
			WriteToLog("Missing GPS config");
			PQclear(res);
			PQfinish(conn);
			return 0;
		}
		rec_count = PQntuples(res);
		if (rec_count) {
			cout_mutex.lock();
			cout << "��������� GPS ������" << endl;
			cout_mutex.unlock();
			//GPS port
			i_fnum = PQfnumber(res, "port_name"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			gps_port = iptr;


			//GPS ��� ����� 
			i_fnum = PQfnumber(res, "gps_data_path"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			gps_file_name = iptr;


			//NMEA ��� �����
			i_fnum = PQfnumber(res, "nmea_data_path"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			nmea_file_name = iptr;


			//�������� ����� ���
			i_fnum = PQfnumber(res, "port_speed"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			port_speed = stoi(iptr, nullptr, 10);


			//��������� �� � ���� GPS
			i_fnum = PQfnumber(res, "gps_data_save"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			gps_data_save = stoi(iptr, nullptr, 10);


			//��������� �� � ���� NMEA
			i_fnum = PQfnumber(res, "nmea_data_save"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
												// ��������� �� ������� �����
			nmea_data_save = stoi(iptr, nullptr, 10);

			// ������ ����� �����
			i_fnum = PQfnumber(res, "time_sentence"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
			if (strlen(iptr) > 0)
				time_sentence = iptr;

			// ������ ����� ����������
			i_fnum = PQfnumber(res, "coord_sentence"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
			if (strlen(iptr) > 0)
				coord_sentence = iptr;

			// ������ ����� ����
			i_fnum = PQfnumber(res, "course_sentence"); // ���������� ���������� ����� �� ����� �������
			iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
			if (strlen(iptr) > 0)
				course_sentence = iptr;


			cout_mutex.lock();
			cout << "���� GPS: " << gps_port << endl;
			cout << "���� GPS: " << gps_file_name << endl;
			cout << "���� NMEA: " << nmea_file_name << endl;
			cout << "�������� �����: " << port_speed << " bit" << endl;
			cout << "��������� GPS � ����: " << gps_data_save << endl;
			cout << "��������� NMEA � ����: " << nmea_data_save << endl;
			cout_mutex.unlock();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	PQclear(res);
	PQfinish(conn);

	return 1;	// ������� �������� ������
}

int ReadConfigfromSQL() {
	PGconn          *conn;
	PGresult        *res;
	char *paramValues[50];
	int paramLenghts[50];
	int paramFormats[50];
	int nParams;
	int             rec_count;
	int             row;
	int             col;
	int	i_fnum;
	int32_t ival[3];
	char       *iptr;
	string str;
	char c;	// ��������� ���������� ��� ����������� true/false
	//ReadConfigFile();
	conn = PQconnectdb(DB_PATH);
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout_mutex.lock();
		cout << "�� ������� ������������ � ����" << endl;
		cout_mutex.unlock();
		WriteToLog("Missing database");
	}
	while (1)
	{
		if (is_running)		// ��� ���� ����� ���, ��������� ������ ������ is_running
		{
			// ���� �� ���� ����� - �������� ���� ������
			res = PQexec(conn, "SELECT is_running FROM public.settings where is_active = true ORDER BY id DESC LIMIT 1");
			if (PQresultStatus(res) != PGRES_TUPLES_OK)
			{
				cout_mutex.lock();
				cout << "������ ��������� ������������ " << endl;
				cout_mutex.unlock();
			}
			rec_count = PQntuples(res);
			if (rec_count) {
				// ������� �� ������/��������� ������ ���
				i_fnum = PQfnumber(res, "is_running"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					if (stoi(iptr, nullptr) == 0)
						is_running = false;
					else
						is_running = true;
			}
		}
		else
		{		/////////////////////////  �������� ����� ������������
			 // ���� �� ���� ����� - �������� ���� ������
			res = PQexec(conn, "SELECT * FROM public.settings where is_active = true ORDER BY id DESC LIMIT 1");
			if (PQresultStatus(res) != PGRES_TUPLES_OK)
			{
				cout_mutex.lock();
				cout << "������ ��������� ������������ " << endl;
				cout_mutex.unlock();
			}
			rec_count = PQntuples(res);
			if (rec_count) {
				//��� ���
				i_fnum = PQfnumber(res, "adc_type"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)							// ��������� �� ������� �����
					adc_type = stoi(iptr, nullptr, 10);
				//��������� ����� ��������
				i_fnum = PQfnumber(res, "last_shot_id"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)									// ��������� �� ������� �����
					project_shot_number = stoi(iptr, nullptr, 10);
				//�������� � ������
				i_fnum = PQfnumber(res, "channel_from"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					channel_from = stoi(iptr, nullptr, 10);
				//����������� �������
				i_fnum = PQfnumber(res, "channel_to"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)							// ��������� �� ������� �����
					channel_to = stoi(iptr, nullptr, 10);
				//������ ���������
				i_fnum = PQfnumber(res, "shot_period"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					shot_period = stoi(iptr, nullptr, 10);
				//������ �������������
				i_fnum = PQfnumber(res, "sample_interval"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)						// ��������� �� ������� �����
					sample_interval = stof(iptr, nullptr);
				//������ ������
				i_fnum = PQfnumber(res, "record_lenght"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					record_lenght = stof(iptr, nullptr);
				//�������� ������
				i_fnum = PQfnumber(res, "record_delay"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					record_delay = stoi(iptr, nullptr);
				if (record_delay < 10) record_delay = 10;	/// �������� �� ����� ���� ������ ������� �������� "�����"
				//GPS 
				i_fnum = PQfnumber(res, "gps_on"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr)>0)
					if (stoi(iptr, nullptr) != 0)
						gps_on = true;
					else
						gps_on = false;
				//������� 
				i_fnum = PQfnumber(res, "depth_on"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					if (stoi(iptr, nullptr) != 0)
						depth_on = true;
					else
						depth_on = false;
				/// ��� �������
				i_fnum = PQfnumber(res, "project_name"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					project_name = iptr;
				/// ������
				i_fnum = PQfnumber(res, "project_object"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					project_object = iptr;
				// ��������
				i_fnum = PQfnumber(res, "project_customer"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					project_customer = iptr;
				// �����������
				i_fnum = PQfnumber(res, "project_performer"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					project_performer = iptr;
				// �������
				i_fnum = PQfnumber(res, "project_ship"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					project_ship = iptr;
				// ������� �����
				i_fnum = PQfnumber(res, "file_prefix"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
					file_prefix = iptr;
				// �������� GPS
				i_fnum = PQfnumber(res, "file_postfix_gpstime"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
				{
					c = iptr[0];
					if (c == 't')
						file_postfix_gpstime = true;

					else
						file_postfix_gpstime = false;
				}
				// �������� - ���������
				i_fnum = PQfnumber(res, "file_postfix_enumerator"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)
				{
					c = iptr[0];
					if (c == 't')
						file_postfix_enumerator = true;
					else
						file_postfix_enumerator = false;
				}
				// ������� �� ������/��������� ������ ���
				i_fnum = PQfnumber(res, "is_running"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					if (stoi(iptr, nullptr) == 0)
						is_running = false;
					else
						is_running = true;

				// ������� �� ������/��������� ������ ���
				i_fnum = PQfnumber(res, "sync_internal"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (iptr != NULL)
				{
					c = iptr[0];
					if (c == 't')
						internal_syncro = true;
					else
						internal_syncro = false;
				}
				// ���������������� ���� ��� ���
				i_fnum = PQfnumber(res, "adc_diff_input"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (iptr != NULL)
				{
					c = iptr[0];
					if (c == 't')
						adc_diff_input = true;
					else
						adc_diff_input = false;
				}

				switch (adc_type)
				{
				case 14:
					if (adc_diff_input & (channel_to > 16))	// � ���. ������ - �������� 16 ������� ���
						channel_to = 16;
					adc14_channels = channel_to - channel_from + 1;	// ���������� ������� ���
					if (adc14_channels < 1) adc14_channels = 1;
					adc14_freq = 1 / sample_interval*adc14_channels;	// ������� ���
					adc14_pages = 2;		// ���������� ������� ������ ��� �� 6144  (0.2c)
					adc_cycles = (record_lenght*adc14_freq) / (3200 * adc14_pages) * 2 + 1;		// ���������� �������� ��� ���������� ������ �������� �� ������� ������, ����� ������������� �������� ��� �����, ����� ����������� ������
					sample_quantity = record_lenght / sample_interval;
					break;
				case 24:
					break;
				}

				// ������ ���������� ����
				i_fnum = PQfnumber(res, "adc_gain"); // ���������� ���������� ����� �� ����� �������
				iptr = PQgetvalue(res, 0, i_fnum);	// �������� ��������� �� ������ ���������� ������������� ��������
				if (strlen(iptr) > 0)								// ��������� �� ������� �����
					streamer_gain = stoi(iptr, nullptr);
			}
		}
		//last_gps.time_string = "12:34:18 12.15.2016";
		/// ���������� ������ � ������� GPS_data � ������ �� ����������

		if ((last_gps.latitude != old_gps.latitude) || (last_gps.longitude != old_gps.longitude))
		{
			union int_float // ����������� ��� ���������� ������������������ ���� �� float
			{
				float fl;
				int in;
			};
			// �������������� ����� �� float ��� �������� real � ��
			int_float u_longitude;
			int_float u_latitude;
			u_longitude.fl = last_gps.longitude;
			u_latitude.fl = last_gps.latitude;
			u_longitude.in = htonl(u_longitude.in);
			u_latitude.in = htonl(u_latitude.in);
			paramValues[0] = (char*)&u_longitude.in;
			paramLenghts[0] = 4;		/// ����� � ������
			paramFormats[0] = 1;
			paramValues[1] = (char*)&u_latitude.in;
			paramLenghts[1] = 4;		/// ����� � ������
			paramFormats[1] = 1;
			paramValues[2] = (char*)&last_gps.time_string;
			paramLenghts[2] = 4;		/// ����� � ������
			paramFormats[2] = 0;

			nParams = 2;
			res = PQexecParams(conn,
				"insert into public.gps_data (longitude, latitude, time_date) values ($1::real, $2::real, 'now')",
				nParams,
				NULL,
				paramValues,
				paramLenghts,
				paramFormats,
				NULL
				);

			if (PQresultStatus(res) != PGRES_COMMAND_OK)
			{
				cout_mutex.lock();
				cout << "������ ������ � �� GPS" << PQerrorMessage(conn) << endl;
				cout_mutex.unlock();
				WriteToLog("Database GPS Writing error");
			}
			else {
				cout_mutex.lock();
				//cout << "������ ������ settings" << endl;
				cout_mutex.unlock();
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));	/// �����, ����� �� ����������� ��
	}
	cout_mutex.lock();
	cout << "����� ������������� ����������!!!!!!!!!!!!!!!!!!!!!" << endl;
	cout_mutex.unlock();
	// ������� ���� ����� ������������ - �� � �������
	//res = PQexec(conn, "UPDATE public.settings SET config_updated=0 where is_active = true");
	PQclear(res);
	PQfinish(conn);
	//str = "Config sucsesful. " + to_string(adc_type) + "; Channels: " + to_string(channel_from) + "-" + to_string(channel_to) + "; Shot period: " + to_string(shot_period) + "; Sample interval: " + to_string(sample_interval) + "; Record lenght:" + to_string(record_lenght) + "; Record delay: " + to_string(record_delay);
	//WriteToLog(str);
	return 1;	// ������� �������� ������
}

int WaitForStart() {
	PGconn          *conn;
	PGresult        *res;
	int             rec_count;
	int             row;
	int             col;
	int	i_fnum;
	int32_t ival[3];
	char       *iptr;
	string insert_string;	// ������ ��� ���������� � ������
	conn = PQconnectdb(DB_PATH);
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout << "�� ������� ������������ � ����" << endl;
	}
	res = PQexec(conn, "SELECT is_running FROM public.settings where is_active = true ORDER BY id DESC LIMIT 1");
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		cout << "������: " << PQerrorMessage(conn) << endl;
	}
	if (PQgetlength(res, 0, 0) > 0) {
		iptr = PQgetvalue(res, 0, 0);	// �������� ��������� �� ������ ���������� ������������� ��������
											// ��������� �� ������� �����
		return (stoi(iptr, nullptr, 10));
	}
	PQclear(res);
	PQfinish(conn);
	return (5);
}
void ClearBase() {
	PGconn          *conn;
	PGresult        *res;
	conn = PQconnectdb(DB_PATH);
	if (PQstatus(conn) == CONNECTION_BAD) {
		cout << "�� ������� ������������ � ����" << endl;
	}
	else {
		res = PQexec(conn, "TRUNCATE public.adc_data");
		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			cout_mutex.lock();
			cout << "������ ������� ���� ���!!!!" << endl;
			cout_mutex.unlock();
		}
		else
			cout << "�������� ���� ���" << endl;
		res = PQexec(conn, "TRUNCATE public.gps_data");
		if (PQresultStatus(res) != PGRES_COMMAND_OK)
		{
			cout_mutex.lock();
			cout << "������ ������� ���� GPS!!!!" << endl;
			cout_mutex.unlock();
		}
		else
			cout << "�������� ���� GPS" << endl;
		PQclear(res);
		PQfinish(conn);
	}
}

int ReadConfigFile() {
	string buf2;
	char buf[100];
	FILE *cfg_file = fopen("sborexview.config", "r");
	fread(buf, 100, 10, cfg_file);
	//cout << buf << endl;
	return 0;
}