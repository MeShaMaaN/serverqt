QT += core
QT -= gui

CONFIG += c++11

TARGET = ServerQT
CONFIG += console
CONFIG -= app_bundle

INCLUDEPATH += C:/boost_1_64_0/
LIBS += "-LC:/boost_1_64_0/stage/lib/"

TEMPLATE = app

SOURCES += \
    gps.cpp \
    netServer.cpp \
    nmea.cpp \
    pressure.cpp \
    segd.cpp \
    serial_port2.cpp \
    Server.CPP \
    sql_postgres.cpp \
    USB_FTDI_ADC.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    ftd2xx.h \
    gps.h \
    netServer.h \
    nmea.h \
    pressure.h \
    segd.h \
    serial_port2.h \
    Server.h \
    sql_postgres.h \
    targetver.h \
    USB_FTDI_ADC.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'../../../../../Program Files (x86)/PostgreSQL/9.5/lib/' -llibpq
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'../../../../../Program Files (x86)/PostgreSQL/9.5/lib/' -llibpqd
else:unix: LIBS += -L$$PWD/'../../../../../Program Files (x86)/PostgreSQL/9.5/lib/' -llibpq

INCLUDEPATH += $$PWD/'../../../../../Program Files (x86)/PostgreSQL/9.5/include'
DEPENDPATH += $$PWD/'../../../../../Program Files (x86)/PostgreSQL/9.5/include'
