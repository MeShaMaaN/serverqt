//---------------------------------------------------------------------------

#ifndef USB_FTDI_ADCH
#define USB_FTDI_ADCH
//---------------------------------------------------------------------------
#include <windows.h>
#include "ftd2xx.h"
#include <string>
#include <mutex>

using namespace std;


//������ ��������� ����� �������� (� �������������) �������� ������� DATA_READY ��� ������� �������� ���
#define DATA_READY_TIMEOUT_VAL_ms   ((500 * Current_ADC_Settings.Mul * Current_ADC_Settings.FreqDiv)/32 + Current_ADC_Settings.Delay + 200)
//---------------------------------------------------------------------------
//������� CS ��� ���� ����������� � ������� �������� ����� ������
#define START_BIT_MASK (1U<<0)              //����� ��� ������������ ������� ����� ���
enum  TBoardNum {                           //����� �������� ������ ����� ���
    DEF_STATE=0xFF,
    BOARD1=~(1U<<0),
    BOARD2=~(1U<<1),
    BOARD3=~(1U<<2),
    BOARD4=~(1U<<3),
    BOARD5=~(1U<<4),
    BOARD6=~(1U<<5),
    BOARD7=~(1U<<6),
    BOARD8=~(1U<<7)};

#define MAX_BOARDS_NUM      8U              //������������ ����� ������������ ���� ���
#define MAX_CHANNELS_NUM    16U             //���������� ������� � ������ ����� ���
#define MAX_SAMPLES_NUM     4000U           //������������ ���������� ������� � ������� ������
#define LEN_OF_ONE_SAMPLE   3U              //������ ����� ������� (����)

#define WR_COMMAND_LEN      4U              //������ ������� "��������" ��� FTDI   (����)
#define RD_COMMAND_LEN      3U              //������ ������� "���������" ��� FTDI  (����)
#define ADC_SETTINGS_LEN    4U              //������ ���������� ������ ���         (����)

#define TX_DELAY_NUM        2U              //���������� ������ ������ ������ ����� ������ ���������� ������ ����� (��������)
#define TX_BUF_LEN  (MAX_BOARDS_NUM*WR_COMMAND_LEN*ADC_SETTINGS_LEN*(TX_DELAY_NUM+1)) //������ ������ ������ �� ������ ��� FTDI

#define TRANSFER_LEN 65536                  //���������� �������� ���� �� 1 ���

//###########################################################################################################

struct T_ADC_Settings{  //��������� ��� �������� ���������� ������ ���
	unsigned int FreqDiv;  //1 ���� - ������� �������������:  F= 32 /FreqDiv  ���,  1<FreqDiv< 16
	unsigned int Mul;      //1 ���� - ��������� ���������� ������� �� ���� �����   N=500*Mul, 1<Mul<8
	unsigned int Delay;    //2 ����� - �������� �������� � �������������: �� 0 �� 10000.
};

typedef INT32 SAMPLES_TYPE;                 //���, � ������� �������� �������

struct TSamplesArray{ //��������� ��� �������� ������� ������� �� ���� ����, ���� �������
    mutex Mutex;
    UINT32 len;                                                             //���������� ������� �� ���� ����� 
    SAMPLES_TYPE Sample[MAX_BOARDS_NUM][MAX_CHANNELS_NUM][MAX_SAMPLES_NUM]; //������
};

#define WAIT_FTDI_ACCESS_TIMEOUT    12000U   //������� �������� ������� � ������ � FTDI (��)
#define WAIT_ARRAY_ACCESS_TIMEOUT   100U     //������� �������� ������� � ������� ������� (��)
//###########################################################################################################
class T_USB_FTDI_ADC
{
public:
    enum {rFalse=0, rTrue, rSuccess, rError};
private:
    T_ADC_Settings Current_ADC_Settings;

    HANDLE hTimer;          //������ ��� ������� �������� �������� ������� DATA_READY
    LARGE_INTEGER liTimeout;//������� �������� ������� DATA_READY

	UINT32 CurArrayLen;		//���������� ������ �������
    HANDLE FT_Mutex;  		//������� ������� � ������ � FTDI
    FT_HANDLE ftHandle;     //���������� FTDI (������ � FTDI �������������� ��� � ������)

    //����� ������ ������ ��� FT2232H
    UINT8 FT2232H_CommandsBuffer[MAX_SAMPLES_NUM*LEN_OF_ONE_SAMPLE*MAX_CHANNELS_NUM*RD_COMMAND_LEN*MAX_BOARDS_NUM];
    //����� ����������� �� USB ������
    UINT8 USB_RX_Buffer[MAX_SAMPLES_NUM*LEN_OF_ONE_SAMPLE*MAX_CHANNELS_NUM*MAX_BOARDS_NUM];

    //������������ �������� �� ���� ��� ������ � ������� � �������� �� � ��������� TSamplesArray
    bool  ConvertSamplesArray(void(*pf)(const string), const unsigned char* Buf, UINT SamplesNum, TSamplesArray* PSamplesArray);

    void  PrintString(void(*pf)(const string), const string str) {if(pf!=NULL) pf(str);}

protected:

    //���������� true, ���� ��������� ���������
    bool  CheckADCParameters(const T_ADC_Settings* PSettings);

    //��������� ������� ���������� ������� DATA_READY �� ���� ���
    UINT32  Check_DATA_READY(void(*pf)(const string));

public:

     T_USB_FTDI_ADC(void(*pf)(const string));  	//�����������

//###########################################################################################################
//#######################        �������� ������ ��� ������ � ������ ���        #############################
//###########################################################################################################

    //���������, ���������� �� ���������� FTDI � �������������� ��, ���� NeedInit==true
    bool  CheckAndInitFTDI(void(*pf)(const string), bool NeedInit, UCHAR io_current);

    //���������� ��������� ������ ���
    bool  SendParam(void(*pf)(const string), const T_ADC_Settings* PSettings);

    //���� ������� �������, ������� ���������� ���� ���, ������ ������ �� ���� ���, ������������ ������
    // � �������� ��� � ����� ArrayBuf
    bool  Start(void(*pf)(const string), TSamplesArray* ArrayBuf);

//###########################################################################################################
};

#endif
