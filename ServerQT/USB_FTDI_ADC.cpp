//---------------------------------------------------------------------------



#include "USB_FTDI_ADC.h"
//#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include <iomanip>


using std::cout;
using std::cin;
using std::endl;


//#pragma comment (lib, "ftd2xx.lib")

//##############################################################################

using namespace std;

//##############################################################################
//##############################################################################
//##############################################################################
//����������� ������ T_USB_FTDI_ADC.
 T_USB_FTDI_ADC::T_USB_FTDI_ADC(void(*pf)(const string))
    : CurArrayLen(1000), ftHandle(NULL)
{
    //��������� ��� ��-��������� (������������ ���������� ��-��������� ����� ����� ���)
    Current_ADC_Settings.FreqDiv=16;
    Current_ADC_Settings.Mul=2;
    Current_ADC_Settings.Delay=10;

    FT_Mutex = CreateMutex(NULL, false, NULL);

    //������������� ������ ������ �� ������ ��� FT2232H
    for(UINT i=0; i<(MAX_SAMPLES_NUM*LEN_OF_ONE_SAMPLE*MAX_CHANNELS_NUM*RD_COMMAND_LEN*MAX_BOARDS_NUM); i+=(RD_COMMAND_LEN*MAX_BOARDS_NUM))
    {
        FT2232H_CommandsBuffer[i+0]=0x91;
        FT2232H_CommandsBuffer[i+1]=BOARD1;
        FT2232H_CommandsBuffer[i+2]=0x00;
        FT2232H_CommandsBuffer[i+3]=0x91;
        FT2232H_CommandsBuffer[i+4]=BOARD2;
        FT2232H_CommandsBuffer[i+5]=0x00;
        FT2232H_CommandsBuffer[i+6]=0x91;
        FT2232H_CommandsBuffer[i+7]=BOARD3;
        FT2232H_CommandsBuffer[i+8]=0x00;
        FT2232H_CommandsBuffer[i+9]=0x91;
        FT2232H_CommandsBuffer[i+10]=BOARD4;
        FT2232H_CommandsBuffer[i+11]=0x00;
        FT2232H_CommandsBuffer[i+12]=0x91;
        FT2232H_CommandsBuffer[i+13]=BOARD5;
        FT2232H_CommandsBuffer[i+14]=0x00;
        FT2232H_CommandsBuffer[i+15]=0x91;
        FT2232H_CommandsBuffer[i+16]=BOARD6;
        FT2232H_CommandsBuffer[i+17]=0x00;
        FT2232H_CommandsBuffer[i+18]=0x91;
        FT2232H_CommandsBuffer[i+19]=BOARD7;
        FT2232H_CommandsBuffer[i+20]=0x00;
        FT2232H_CommandsBuffer[i+21]=0x91;
        FT2232H_CommandsBuffer[i+22]=BOARD8;
        FT2232H_CommandsBuffer[i+23]=0x00;
    }

    hTimer = CreateWaitableTimer(NULL, TRUE, NULL);
    if(hTimer == NULL)
    {
        cout << "T_USB_FTDI_ADC::T_USB_FTDI_ADC: CreateWaitableTimer failed! GetLastError() == " << GetLastError() << endl;
    }
}

//------------------------------------------------------------------------------
//���������, ���������� �� ���������� FTDI � �������������� ��, ���� NeedInit==true.
//����������� ���������:
// - pf - ��������� �� ������� ��� ������ ����� ��������� � ���������� ����������;
//		  ���� �� ������������, ������� NULL � ������ ���������.
// - NeedInit - true ��������� �� ������������� ������������� FTDI, ���� ���
//              ����� ����������.
// - io_current - �������� ����������� ����������� ���������� FTDI (mA)
//������������ ��������:
// - true  - ���� FTDI ���������� (� ������� ����������������, ��� NeedInit==true).
// - false - ���� FTDI �� ����������, ��� �� �� ������� ����������������, ��� ���� ������.
bool  T_USB_FTDI_ADC::CheckAndInitFTDI(void(*pf)(const string), const bool NeedInit, UCHAR io_current)
{
    switch(io_current)  //����������� ����������� FTDI � ������������ (���������� ��������: 4, 8, 12, 16)
    {
    case 4:
    case 8:
    case 12:
    case 16:
        break;
    default:
        cout << "������! ������ ������������ �������� ����������� ����������� FTDI! (" << io_current << " mA)" << endl;
        cout << "        ���������� ��������: 4, 8, 12, 16.";
        io_current=4;
		cout << "        ����������� �������� ��-���������: " << io_current << " mA" << endl;
        break;
    }

	FT_STATUS ftStatus;
    DWORD BytesWritten=0;
    DWORD Result;

    //������ ������� � FTDI
    Result = WaitForSingleObject(FT_Mutex, WAIT_FTDI_ACCESS_TIMEOUT);
    if(Result!=WAIT_OBJECT_0)
    {
        if(Result==WAIT_TIMEOUT)
            cout << "CheckAndInitFTDI: �� ������� ��������� ������� FT_Mutex �� ����� " << WAIT_FTDI_ACCESS_TIMEOUT << " ��!" << endl;
        else
        {
            cout << "CheckAndInitFTDI: �� ������� ��������� ������� FT_Mutex! WaitForSingleObject ������� ��� 0x" << setfill('0') << setw(sizeof(Result) * 2)	<< hex << Result << endl;
            if(Result == WAIT_FAILED) 
				cout << " GetLastError() == " << GetLastError() << endl;
        }
        return false;
    }

    DWORD numDevs=0;
    UCHAR cmd_tmp1[]={0x8A};                    //������� ��������� ��������
    UCHAR cmd_tmp2[]={0x82,0x00,START_BIT_MASK};//��������� I/O0 � �������� ������ (���. ������� "0"
    char* Manufacturer   = NULL;
    char* ManufacturerId = NULL;
    char* Description    = NULL;
    char* SerialNumber   = NULL;
    PFT_EEPROM_2232H Pft_eeprom_2232h = NULL;
    FT_DEVICE_LIST_INFO_NODE *devInfo = NULL;
    DWORD dwDriverVer=0;
    UINT FT2232H_num=0;
    bool success = false;

    //����� ������������ ��������� FTDI
    ftStatus = FT_CreateDeviceInfoList(&numDevs);     //����� ������������ ��������� FTDI
    if(!((ftStatus==FT_OK)&&(numDevs>0))) goto free_and_exit;
    devInfo = new FT_DEVICE_LIST_INFO_NODE[numDevs];
    if(FT_GetDeviceInfoList(devInfo,&numDevs)==FT_OK)
        for(DWORD i=0; i<numDevs; i++) if(devInfo[i].Type==FT_DEVICE_2232H) FT2232H_num++; //������� ������������ FT2232H
    delete[] devInfo;
    if(FT2232H_num<2) goto free_and_exit;       		//FT2232 ������������ � ������� ��� 2 ����������� ����������!!!
    if(!NeedInit) {success=true; goto free_and_exit;}   //���� �� ����� ���������������� FTDI
    cout << "���������� ���������: "<< numDevs << ", �� ��� FT2232H: " << FT2232H_num << endl;
    ftStatus = FT_Open(0,&ftHandle);
    if (ftStatus != FT_OK) {cout << "CheckAndInitFTDI: ������! �� ������� ������� ����������!"; goto free_and_exit;}

    //������������� FTDI
    ftStatus = FT_GetDriverVersion(ftHandle,&dwDriverVer);
    if (ftStatus == FT_OK)
    {
        INT32 dv = dwDriverVer;
        cout << "������ ��������: " << ((dv>>16)&0xFF) << "." << ((dv>>8)&0xFF) << "." << (dv&0xFF) << endl;
        const INT32 MinimalDriverVersion = 0x00020824;
        if(dwDriverVer<MinimalDriverVersion)
        {
            cout << "��������: ������������ ���������� ������ ��������!";
            cout << "���������� ������ ��������� �� �������������!";
            cout << "������ �������� ������ ���� �� ������ " << ((MinimalDriverVersion>>16)&0xFF) << "." << ((MinimalDriverVersion>>8)&0xFF) << "." << (MinimalDriverVersion&0xFF) << endl;
        }
    }
    else {cout << "������! �� ������� ���������� ������ ��������!" << endl; goto free_and_exit;}
    ftStatus = FT_SetBitMode(ftHandle,0x00,FT_BITMODE_MCU_HOST);
    if(ftStatus != FT_OK) {cout << "������! �� ������� ������ ����� ������ ����������!" << endl; goto free_and_exit;}
    ftStatus = FT_ResetDevice(ftHandle);
    if (ftStatus != FT_OK) {cout << "������! �� ������� �������� ����������!" << endl; goto free_and_exit;}
    ftStatus = FT_SetLatencyTimer(ftHandle, 2);
    if(ftStatus!=FT_OK) {cout << "������  ��� ������� ���������������� LatencyTimer!" << endl; goto free_and_exit;}
    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 0);
    if(ftStatus!=FT_OK) {cout << "������  ��� ������� ������ ������ �������� ��������!" << endl; goto free_and_exit;}
    ftStatus = FT_SetTimeouts(ftHandle, 3000, 3000); // 3� ����� �������� �� ������ � ������ �� USB
    if(ftStatus!=FT_OK) {cout << "������ ��� ������� ������ ������� �� ����� ������ ������� �������!" << endl; goto free_and_exit;}
    ftStatus = FT_Write(ftHandle, cmd_tmp1, sizeof(cmd_tmp1), &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=sizeof(cmd_tmp1)) {cout << "������ ��� ������� ��������� �������� ������� �� 5!" << endl; goto free_and_exit;}
    ftStatus = FT_Write(ftHandle, cmd_tmp2, sizeof(cmd_tmp2), &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=sizeof(cmd_tmp2)) {cout << "������ ��� ������� ���������� ��������� �������� � ����������� ����� I/O0 � I/O1 !" << endl; goto free_and_exit;}
    //-------------------------------------
    //�������������� ���������� EEPROM FT2232H, ���� ����������
    Manufacturer   = new char[64];
    ManufacturerId = new char[64];
    Description    = new char[64];
    SerialNumber   = new char[64];
    Pft_eeprom_2232h = new FT_EEPROM_2232H;
    Pft_eeprom_2232h->common.deviceType = FT_DEVICE_2232H;
    ftStatus = FT_EEPROM_Read(ftHandle, Pft_eeprom_2232h, sizeof(FT_EEPROM_2232H),
                                Manufacturer, ManufacturerId, Description, SerialNumber);
    if(ftStatus!=FT_OK) cout << "������ ��� ������ �� EEPROM!" << endl;
    else
    {
        if( (Pft_eeprom_2232h->ALDriveCurrent != io_current)||  //���� ����������� ����������� ������� �� ������������ �����������
            (Pft_eeprom_2232h->AHDriveCurrent != io_current)||
            (Pft_eeprom_2232h->BLDriveCurrent != io_current)||
            (Pft_eeprom_2232h->BHDriveCurrent != io_current))
        {//��������� ����������� ����������� ����������� ������� FT2232H
            Pft_eeprom_2232h->ALDriveCurrent = io_current;
            Pft_eeprom_2232h->AHDriveCurrent = io_current;
            Pft_eeprom_2232h->BLDriveCurrent = io_current;
            Pft_eeprom_2232h->BHDriveCurrent = io_current;
            ftStatus = FT_EEPROM_Program(ftHandle, Pft_eeprom_2232h, sizeof(FT_EEPROM_2232H), Manufacturer, ManufacturerId, Description, SerialNumber);
            if(ftStatus!=FT_OK) cout << "������ ��� ������ � EEPROM!" << endl;
            else {success=true; cout << "������ ����������� ����������� " << io_current << " mA" << endl;}
        }
        else success=true;
    }
    delete   Pft_eeprom_2232h;
    delete[] SerialNumber;
    delete[] Description;
    delete[] ManufacturerId;
    delete[] Manufacturer;
    //-------------------------------------
    cout << " ";
free_and_exit:
    ReleaseMutex(FT_Mutex); //������������ ������� � FTDI
    return success;
}

//------------------------------------------------------------------------------
//��������� ������������ ���������� ������ ���.
//����������� ���������:
// - FreqDiv - �������� ������� ����������� ���.
// - SamplesMul - ��������� ���������� �������.
// - StartDelay - �������� ������� ���.
//������������ ��������:
// - true - ���� ��������� ���������.
// - false - ���� ����������� �������� ���� �� ������ �� ����������.
bool  T_USB_FTDI_ADC::CheckADCParameters(const T_ADC_Settings* PSettings)
{
    if(PSettings->FreqDiv==0) return false;
    if(PSettings->Mul==0 || PSettings->Mul>8) return false;
    if(PSettings->Delay>10000) return false;
    return true;
}

//------------------------------------------------------------------------------
//���������� ��������� ������ ���, ���� ��� ���������.
//����������� ���������:
// - pf - ��������� �� ������� ��� ������ ����� ��������� � ���������� ����������;
//		  ���� �� ������������, ������� NULL � ������ ���������.
// - FreqDiv    - �������� ������� ����������� ���.
// - SamplesMul - ��������� ���������� �������.
// - StartDelay - �������� ������� ���.
//������������ ��������:
// - true  - ���� ��������� ����������.
// - false - ���� ����������� �������� ���� �� ������ �� ����������,
//           ��� ��������� ���� ������.
bool  T_USB_FTDI_ADC::SendParam(void(*pf)(const string), const T_ADC_Settings* PSettings)
{
    static const UCHAR board_num[MAX_BOARDS_NUM]={BOARD1,BOARD2,BOARD3,BOARD4,BOARD5,BOARD6,BOARD7,BOARD8};
    static UCHAR param_val[ADC_SETTINGS_LEN];

    FT_STATUS ftStatus;
    DWORD BytesWritten=0;
    DWORD Result;

    if(!CheckADCParameters(PSettings)){cout << "SendParam: ������! ������������ ��������� ������ ���!" << endl; return false;}
    if(ftHandle==NULL){cout << "SendParam: ������! ���������� FTDI �� ���������������!" << endl; return false;}

    //������ ������� � FTDI
    Result = WaitForSingleObject(FT_Mutex, WAIT_FTDI_ACCESS_TIMEOUT);
    if(Result!=WAIT_OBJECT_0)
    {
        if(Result==WAIT_TIMEOUT)
            cout << "SendParam: �� ������� ��������� ������� FT_Mutex �� ����� " << WAIT_FTDI_ACCESS_TIMEOUT<< " ��!" << endl;
        else
        {
            cout << "SendParam: �� ������� ��������� ������� FT_Mutex! WaitForSingleObject ������� ��� 0x" << setfill('0') << setw(sizeof(Result) * 2) << hex << Result << endl;
            if(Result == WAIT_FAILED) cout << " GetLastError() == " << GetLastError() << endl;
        }
        return false;
    }

    //������������ ��� FTDI ������������������ ������, �������������� ������ ���������� � ����� ���
    param_val[0]=(UCHAR)PSettings->FreqDiv;
    param_val[1]=(UCHAR)PSettings->Mul;
    param_val[2]=(UCHAR)PSettings->Delay;
    param_val[3]=(UCHAR)(PSettings->Delay >> 8);
    UCHAR* buf = new unsigned char[TX_BUF_LEN];
    UINT i=0;
    for(UINT p=0; p<ADC_SETTINGS_LEN; p++)
    {
        for(UINT j=0; j<MAX_BOARDS_NUM; j++)
        {
            buf[i+0]=0x93;
            buf[i+1]=board_num[j];
            buf[i+2]=0x00;
            buf[i+3]=param_val[p];
            i+=WR_COMMAND_LEN;
            for(UINT k=0; k<TX_DELAY_NUM; k++)
            {
                buf[i+0]=0x93;
                buf[i+1]=DEF_STATE;
                buf[i+2]=0x00;
                buf[i+3]=0x00;
                i+=WR_COMMAND_LEN;
            }
        }
    }

    //�������� �������������� ������ � FTDI
    ftStatus = FT_Write(ftHandle, buf, TX_BUF_LEN, &BytesWritten);
    delete[] buf;
    bool success = true;
    if(ftStatus!=FT_OK || BytesWritten!=TX_BUF_LEN) {cout << "������ ��� �������� ���������� ������ ���!" << endl; success=false;}
    CurArrayLen = PSettings->Mul*500;

    Current_ADC_Settings.FreqDiv = PSettings->FreqDiv;
    Current_ADC_Settings.Mul = PSettings->Mul;
    Current_ADC_Settings.Delay = PSettings->Delay;
    ReleaseMutex(FT_Mutex); //������������ ������� � FTDI
    return success;
}

//------------------------------------------------------------------------------
//��������� � ������������ ������ �� ������ Buf (������ ������ ���� � ��� ����, � �����
// ��� ���� ������� �� FTDI) � �������, � �������� � ��������� TSamplesArray.
//����������� ���������:
// - Buf - ��������� �� �����, ���������� �������� �� FTDI ������.
// - SamplesNum - ���������� ������� �� �����, ������� ���������� ��������.
// - PSamplesArray - ��������� �� ��������� TSamplesArray ��� �����������.
//������������ ��������:
// - true  - ������ ������������ � ���������� � TSamplesArray ���������.
// - false - �� ������� ��������� ������ � �������.
bool  T_USB_FTDI_ADC::ConvertSamplesArray(void(*pf)(const string), const unsigned char* Buf, const UINT SamplesNum, TSamplesArray* PSamplesArray)
{
	PSamplesArray->Mutex.lock();
		/*, WAIT_ARRAY_ACCESS_TIMEOUT);
    if(Result!=WAIT_OBJECT_0)
    {
        if(Result==WAIT_TIMEOUT)
            cout << "ConvertSamplesArray: �� ������� ��������� ������� PSamplesArray->Mutex �� ����� " << WAIT_ARRAY_ACCESS_TIMEOUT << " ��!";
        else
        {
            cout << "ConvertSamplesArray: �� ������� ��������� ������� PSamplesArray->Mutex! WaitForSingleObject ������� ��� 0x" << setfill('0') << setw(sizeof(Result) * 2) << hex << Result << endl;
            if(Result == WAIT_FAILED) cout << " GetLastError() == " << GetLastError() << endl;
        }
        return false;
    }
	*/
    for(UINT BdNum=0; BdNum<MAX_BOARDS_NUM; BdNum++)
        for(UINT ChNum=0; ChNum<MAX_CHANNELS_NUM; ChNum++)
            for(UINT i=0; i<SamplesNum; i++)
            {
                UINT32 shift = BdNum+(MAX_BOARDS_NUM*3*i)+(MAX_BOARDS_NUM*3*SamplesNum*ChNum);
                INT32 tmp =((INT32)Buf[shift+(MAX_BOARDS_NUM*0)])<<24;
                tmp|=((INT32)Buf[shift+(MAX_BOARDS_NUM*1)])<<16;
                tmp|=((INT32)Buf[shift+(MAX_BOARDS_NUM*2)])<<8;
                PSamplesArray->Sample[BdNum][ChNum][i]=tmp/256;
            }
    PSamplesArray->len=SamplesNum;
    //ReleaseMutex(PSamplesArray->Mutex);
	PSamplesArray->Mutex.unlock();
    return true;
}

//------------------------------------------------------------------------------
//��������� ������� ���������� ������� DATA_READY �� ���� ���.
//����������� ���������:
// - pf - ��������� �� ������� ��� ������ ����� ��������� � ���������� ����������;
//		  ���� �� ������������, ������� NULL � ������ ���������.
//������������ ��������:
// - rTrue - ���� ������ DATA_READY.
// - rFalse - ��� ������� DATA_READY.
// - rError - �� ������� ��������� ��������� ������� DATA_READY.
UINT32  T_USB_FTDI_ADC::Check_DATA_READY(void(*pf)(const string))
{
    FT_STATUS ftStatus;
    DWORD BytesWritten=0;
    DWORD BytesReturned=0;

    UINT8 tmp = 0x83;           //������� FTDI ��������� ����
    ftStatus = FT_Write(ftHandle, &tmp, 1, &BytesWritten);
    if (ftStatus!=FT_OK || BytesWritten!=1) {cout << "������ ��� �������� ������� ������ ��������� ������� DATA_READY!" << endl; return rError;}
    ftStatus = FT_Read(ftHandle, &tmp, 1, &BytesReturned);
    if (ftStatus!=FT_OK || BytesReturned!=1){cout << "������ ��� ������ ��������� ������� DATA_READY!" << endl; return rError;}
    if(tmp&(1U<<1)) return rTrue; //���� ���� ������ DATA_READY (���������� � 1)
    else return rFalse;
}

//------------------------------------------------------------------------------
//���� ������� �������, ������� ���������� ���� ���, ������ ������ �� ���� ���, ������������ ������
// � �������� ��� � ����� ArrayBuf.
//����������� ���������:
// - pf - ��������� �� ������� ��� ������ ����� ��������� � ���������� ����������;
//		  ���� �� ������������, ������� NULL � ������ ���������.
// - ArrayBuf - ��������� �� ����� ��� ������� �������.
//������������ ��������:
// - true - ������ ������� ������� ������ � ���������� � ����� ArrayBuf.
// - false - ��������� ������.
bool  T_USB_FTDI_ADC::Start(void(*pf)(const string), TSamplesArray* ArrayBuf)
{
	//������ ������ ��� FTDI, ����������� ������� �������
    static unsigned char start_pulse[]={
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x93,DEF_STATE,0x00,0x00,
    0x82,START_BIT_MASK,START_BIT_MASK,
    0x82,START_BIT_MASK,START_BIT_MASK,
    0x82,START_BIT_MASK,START_BIT_MASK,
    0x82,START_BIT_MASK,START_BIT_MASK,
    0x82,START_BIT_MASK,START_BIT_MASK,
    0x82,0x00,START_BIT_MASK};

    FT_STATUS ftStatus;
    DWORD BytesWritten=0;
    DWORD BytesReturned=0;
    DWORD Result;

    if(ArrayBuf==NULL){cout << "Start: �� ������� ��������� ������� FT_Mutex ��  ��!" << endl; return false;}
    if(ftHandle==NULL){cout << "Start: ������! ���������� FTDI �� ���������������!" << endl; return false;}

    //������ ������� � FTDI
    Result = WaitForSingleObject(FT_Mutex, WAIT_FTDI_ACCESS_TIMEOUT);
    if(Result!=WAIT_OBJECT_0)
    {
        if(Result==WAIT_TIMEOUT)
            cout << "Start: �� ������� ��������� ������� FT_Mutex �� ����� " << WAIT_FTDI_ACCESS_TIMEOUT << " ��!" << endl;
        else
        {
            cout << "Start: �� ������� ��������� ������� FT_Mutex! WaitForSingleObject ������� ��� 0x" << setfill('0') << setw(sizeof(Result) * 2) << hex <<  Result << endl;
            if(Result == WAIT_FAILED) cout << " GetLastError() == " << GetLastError() << endl;
        }
        return false;
    }

    //������ �������� �������
    ftStatus = FT_Write(ftHandle, start_pulse, sizeof(start_pulse), &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=sizeof(start_pulse))
    {
        cout << "������ ��� �������� ������ ������������ �������� �������!";
        ReleaseMutex(FT_Mutex);	//������������ ������� � FTDI
        return false;
    }
    //�������� ������� DATA_READY
    liTimeout.QuadPart = -10000LL*DATA_READY_TIMEOUT_VAL_ms;    //����� ����� ��������
    if(!SetWaitableTimer(hTimer, &liTimeout, 0, NULL, NULL, 0)) //������������� ������� ������� �������� �������� DATA_READY
    {
        cout << "T_USB_FTDI_ADC::Start: SetWaitableTimer failed! GetLastError() == " << GetLastError() << endl;
        ReleaseMutex(FT_Mutex);	//������������ ������� � FTDI
        return false;
    }
    UINT32 CheckResult=false;
    while(1)
    {
        Sleep(2);
        CheckResult = Check_DATA_READY(pf);
        if(CheckResult!=rFalse) break;
        //�������� ��������
        Result = WaitForSingleObject(hTimer, 0);
        if(Result==WAIT_OBJECT_0) break;
    }
    if(Result==WAIT_OBJECT_0)
    {
        cout << "������! ������� ����� �������� ������� DATA_READY (" << liTimeout.QuadPart/-10000LL << " ��)!" << endl;
        ReleaseMutex(FT_Mutex);	//������������ ������� � FTDI
        return false;
    }
    if(CheckResult!=rTrue){ReleaseMutex(FT_Mutex); return false;} //�����, ���� ���� ������

    //������ �������� �� TRANSFER_LEN ����
    UINT bytes_to_send = CurArrayLen*LEN_OF_ONE_SAMPLE*MAX_CHANNELS_NUM*RD_COMMAND_LEN*MAX_BOARDS_NUM;
    UINT8* usb_buf_ptr = USB_RX_Buffer;
    UINT8* cmd_buf_ptr = FT2232H_CommandsBuffer;
    #define TRANSFER_CMD_RD_LEN     (TRANSFER_LEN*RD_COMMAND_LEN)
    bool success = true;
    while(bytes_to_send>0)
    {
        UINT n;
        if(bytes_to_send>TRANSFER_CMD_RD_LEN) {n=TRANSFER_CMD_RD_LEN; bytes_to_send-=TRANSFER_CMD_RD_LEN;}
        else {n=bytes_to_send; bytes_to_send=0;}
        ftStatus = FT_Write(ftHandle, cmd_buf_ptr, n, &BytesWritten);
        if (ftStatus!=FT_OK || BytesWritten!=n){cout << "������ ��� �������� � FTDI ������ �� ������!\n ����������: " << BytesWritten << endl; success=false; break;}
        cmd_buf_ptr+=n;
        n/=RD_COMMAND_LEN;
        ftStatus = FT_Read(ftHandle, usb_buf_ptr, n, &BytesReturned);
        if (ftStatus!=FT_OK || BytesReturned!=n){cout << "������ ��� ������ ������� �������!" << endl; success=false; break;}
        usb_buf_ptr+=n;
    }

    //���������� � �������������� ��������� ������� �������
    if(success) ConvertSamplesArray(pf, USB_RX_Buffer, CurArrayLen, ArrayBuf);

    //�������� ������ ������� DATA_READY
    //while(Check_DATA_READY(pf)==rTrue) Sleep(1);
    Sleep(1);
    if(Check_DATA_READY(pf)) cout << "������! ����� ��������� ������� �� ���� ������ DATA_READY!" << endl;

    //������������ ������� � FTDI
    ReleaseMutex(FT_Mutex);
    
    return success;
}
