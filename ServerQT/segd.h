#pragma once
#include <cstdint>
#include <mutex>

#define EXT_HEADER_SIZE 0
#define EXTD_HEADER_SIZE 50*32

struct DatasArray { //��������� ��� �������� ������� ������� �� ���� ����, ���� �������
	std::mutex Mut;
	uint32_t len;   //���������� ������� �� ���� ����� 
	float Data[32][25000]; //������
};
class SegD {
public:
	
	/// 1.	GENERAL HEADER  - 32�����.
	char gen_header[32];		// ������ ��� ��������� ���������
	uint16_t* file_number;		// 1-2 � �����(������������� � �������.����)
	uint16_t* format_code;	// 3-4 ��� �������(������������� � �������.����)
	uint16_t* profile_name;	//	5-6 ��� �������(������������� � �������.����)
	uint16_t* shot_piket;	// 7-8 ����� ������ ������(������������� � �������.����)
	uint8_t * year;				//  11 ���
	uint8_t * day2;				// 12 ����(���������) ������� ����
	uint8_t * day;				// 13 ����(���������)
	uint8_t * hour;				// 14 ���
	uint8_t * minute;			// 15 ������
	uint8_t * second;			// 16 �������
	uint8_t* manufacter_code;	// 17 ��� �������������
	uint8_t * base_interval;		//	23	������� �������� ������
	uint8_t * polarity;			//	24	����������
	uint8_t * record_type;		//	26	��� ������
	uint8_t * p_record_lenght;		//	27	����� ������
	uint8_t * ask_type;			//	28	��� ������\������
	uint8_t * channel_set;		//	29	������ �������\��� ������
	uint8_t * mismatch_block;	//	30	����� ���������������
	uint8_t * extended_header;	//	31	���� ������������ ���������
	uint8_t * external_header;	//	32	���� �������� ���������

	/// 1.	GENERAL HEADER #2 - 32�����.
	char gen_header2[32];		// ������ ��� ��������� ���������
	uint16_t* ex_file_number;		// 1-2-3 � �����(������������� � �������.����)
	uint16_t* extended_blocks;	// 6-7 ����� ������������ ��������� � 32 ������ ������
	uint16_t* external_blocks;	// 6-7 ����� ������������ ��������� � 32 ������ ������
	uint8_t * rev1;				//  11 ������ ������� segD
	uint8_t * rev2;				// 12
	uint16_t * ext_record_lenght;				// 15-16-17 Extended Record Length ms 
	uint8_t * gen_header_number;				// 19	General Header Block Number

	////	SCAN TYPE HEADER.CHANEL SET 1 - 32�����.
	char scan_header[32];		// ������ ��� ��������� ���������
	uint8_t * q_type;			//	1	� ���� ������
	uint8_t * channel_set_num;	//	2	� ������ �������
	uint16_t*	begining_time;	//	3 - 4	 ����� ������ ������ �������
	uint16_t*	ending_time;	// 5 - 6	����� ����� ������ �������
	uint16_t*	mr_factor;		// 7 - 8	MP ������
	uint16_t*	channels_quantity;	//9 - 10	 ����� �������(� ���������� �������������)
	uint8_t * channel_type;			//	11	��� ������
	uint8_t * channel_gain;			//	12	�������� ������
	uint16_t*	antialiasing_frqns;	//	13 - 14	������� ����������������� �������(� ���������� �������������)
	uint16_t*	antialiasing_slope;	// 15 - 16	�������� ����������������� �������(� ���������� �������������)
	uint16_t*	h_freq_filter;		// 17 - 18	���(� ���������� �������������)
	uint16_t*	h_freq_filter_slope;	// 19 - 20	�������� ���(� ���������� �������������)

	///	LINE DESCRIPTOR HEADER - 32�����.
	char line_header[32];		// ������ ��� ��������� �������
	uint16_t* file_number_ex;	/// 1 - 3	����������� ����� �����
	uint16_t* profile_number;	///	4 - 6	����� �������
	uint16_t* stirring_piket;	//	9 - 11	����� ������ ����������� !!!!!!!!!!!!!!!!����� ��� 3 �����
	uint8_t * stirrer_set;			//	20	����� ������ ����������

	/// TRACE HEADER - 20����.
	char trace_header[20];		// ������ ��� ��������� ������
	uint16_t* trace_file_number;				/// 1 - 2	����� ����� (���� ��� �����)
	uint8_t * inter_type_number;			//	3	����� ���� ������
	uint8_t* trace_channel_set_num;			//	4	����� ������ ������� (���� ��� �����)
	uint16_t* track_number;			//	5 - 6	����� ������
	uint8_t* sample_skew;		// 11 fractional part of the base scan interval
	//uint16_t shot_piket;			//	17 - 18	����� ������ ������ (���� ��� �����)
	uint16_t* ex_trace_file_number;			//	19 - 20	�����  ������ ������

	/// Extended Header 32 ��

	char v[1600] = "";
	char* extended_header_string = v;

	DatasArray FloatData;
    SegD();
	void WriteSegDfrom14(void);		// ����� � ���� �������� � 14 ������� ���
	void Init(void);
	uint8_t CharToHexDec(uint8_t);
	uint8_t IntToCharLow(uint16_t);
	uint8_t IntToCharHigh(uint16_t);
	uint8_t Collect2Char(uint8_t, uint8_t);
	uint16_t IntToHexDec(uint16_t);

};

void FloatToIEEE(float *);
